// Benchmark "c17" written by ABC on Fri Oct  4 12:36:59 2019

module c17 ( 
    G1, G2, G3, G6, G7,
    G22, G23  );
  input  G1, G2, G3, G6, G7;
  output G22, G23;
  wire new_n8_, new_n9_, new_n10_, new_n11_, new_n13_, new_n14_;
  nand02 g0(.A0(G3), .A1(G1), .Y(new_n8_));
  inv01  g1(.A(G2), .Y(new_n9_));
  nand02 g2(.A0(G6), .A1(G3), .Y(new_n10_));
  nand02 g3(.A0(new_n10_), .A1(new_n9_), .Y(new_n11_));
  xor2   g4(.A0(new_n11_), .A1(new_n8_), .Y(G22));
  inv01  g5(.A(G7), .Y(new_n13_));
  and02  g6(.A0(new_n10_), .A1(new_n13_), .Y(new_n14_));
  xor2   g7(.A0(new_n14_), .A1(new_n11_), .Y(G23));
endmodule


