// Benchmark "c432.blif" written by ABC on Mon Apr  8 18:01:40 2019

module c432  ( 
    G1gat, G4gat, G8gat, G11gat, G14gat, G17gat, G21gat, G24gat, G27gat,
    G30gat, G34gat, G37gat, G40gat, G43gat, G47gat, G50gat, G53gat, G56gat,
    G60gat, G63gat, G66gat, G69gat, G73gat, G76gat, G79gat, G82gat, G86gat,
    G89gat, G92gat, G95gat, G99gat, G102gat, G105gat, G108gat, G112gat,
    G115gat,
    G223gat, G329gat, G370gat, G421gat, G430gat, G431gat, G432gat  );
  input  G1gat, G4gat, G8gat, G11gat, G14gat, G17gat, G21gat, G24gat,
    G27gat, G30gat, G34gat, G37gat, G40gat, G43gat, G47gat, G50gat, G53gat,
    G56gat, G60gat, G63gat, G66gat, G69gat, G73gat, G76gat, G79gat, G82gat,
    G86gat, G89gat, G92gat, G95gat, G99gat, G102gat, G105gat, G108gat,
    G112gat, G115gat;
  output G223gat, G329gat, G370gat, G421gat, G430gat, G431gat, G432gat;
  wire G118gat, G119gat, G122gat, G123gat, G126gat, G127gat, G130gat,
    G131gat, G134gat, G135gat, G138gat, G139gat, G142gat, G143gat, G146gat,
    G147gat, G150gat, G151gat, G154gat, G157gat, G158gat, G159gat, G162gat,
    G165gat, G168gat, G171gat, G174gat, G177gat, G180gat, G183gat, G184gat,
    G185gat, G186gat, G187gat, G188gat, G189gat, G190gat, G191gat, G192gat,
    G193gat, G194gat, G195gat, G196gat, G197gat, G198gat, G199gat, G203gat,
    G213gat, G224gat, G227gat, G230gat, G233gat, G236gat, G239gat, G242gat,
    G243gat, G246gat, G247gat, G250gat, G251gat, G254gat, G255gat, G256gat,
    G257gat, G258gat, G259gat, G260gat, G263gat, G264gat, G267gat, G270gat,
    G273gat, G276gat, G279gat, G282gat, G285gat, G288gat, G289gat, G290gat,
    G291gat, G292gat, G293gat, G294gat, G295gat, G296gat, G300gat, G301gat,
    G302gat, G303gat, G304gat, G305gat, G306gat, G307gat, G308gat, G309gat,
    G319gat, G330gat, G331gat, G332gat, G333gat, G334gat, G335gat, G336gat,
    G337gat, G338gat, G339gat, G340gat, G341gat, G342gat, G343gat, G344gat,
    G345gat, G346gat, G347gat, G348gat, G349gat, G350gat, G351gat, G352gat,
    G353gat, G354gat, G355gat, G356gat, G357gat, G360gat, G371gat, G372gat,
    G373gat, G374gat, G375gat, G376gat, G377gat, G378gat, G379gat, G380gat,
    G381gat, G386gat, G393gat, G399gat, G404gat, G407gat, G411gat, G414gat,
    G415gat, G416gat, G417gat, G418gat, G419gat, G420gat, G422gat, G425gat,
    G428gat, G429gat;
  assign G223gat = ~G199gat;
  assign G329gat = ~G296gat;
  assign G370gat = ~G357gat;
  assign G421gat = ~G415gat & ~G416gat;
  assign G430gat = ~G422gat | ~G399gat | ~G381gat | ~G386gat;
  assign G431gat = ~G425gat | ~G428gat | ~G381gat | ~G386gat;
  assign G432gat = ~G425gat | ~G429gat | ~G381gat | ~G422gat;
  assign G118gat = ~G1gat;
  assign G119gat = ~G4gat;
  assign G122gat = ~G11gat;
  assign G123gat = ~G17gat;
  assign G126gat = ~G24gat;
  assign G127gat = ~G30gat;
  assign G130gat = ~G37gat;
  assign G131gat = ~G43gat;
  assign G134gat = ~G50gat;
  assign G135gat = ~G56gat;
  assign G138gat = ~G63gat;
  assign G139gat = ~G69gat;
  assign G142gat = ~G76gat;
  assign G143gat = ~G82gat;
  assign G146gat = ~G89gat;
  assign G147gat = ~G95gat;
  assign G150gat = ~G102gat;
  assign G151gat = ~G108gat;
  assign G154gat = ~G118gat | ~G4gat;
  assign G157gat = ~G8gat & ~G119gat;
  assign G158gat = ~G14gat & ~G119gat;
  assign G159gat = ~G122gat | ~G17gat;
  assign G162gat = ~G126gat | ~G30gat;
  assign G165gat = ~G130gat | ~G43gat;
  assign G168gat = ~G134gat | ~G56gat;
  assign G171gat = ~G138gat | ~G69gat;
  assign G174gat = ~G142gat | ~G82gat;
  assign G177gat = ~G146gat | ~G95gat;
  assign G180gat = ~G150gat | ~G108gat;
  assign G183gat = ~G21gat & ~G123gat;
  assign G184gat = ~G27gat & ~G123gat;
  assign G185gat = ~G34gat & ~G127gat;
  assign G186gat = ~G40gat & ~G127gat;
  assign G187gat = ~G47gat & ~G131gat;
  assign G188gat = ~G53gat & ~G131gat;
  assign G189gat = ~G60gat & ~G135gat;
  assign G190gat = ~G66gat & ~G135gat;
  assign G191gat = ~G73gat & ~G139gat;
  assign G192gat = ~G79gat & ~G139gat;
  assign G193gat = ~G86gat & ~G143gat;
  assign G194gat = ~G92gat & ~G143gat;
  assign G195gat = ~G99gat & ~G147gat;
  assign G196gat = ~G105gat & ~G147gat;
  assign G197gat = ~G112gat & ~G151gat;
  assign G198gat = ~G115gat & ~G151gat;
  assign G199gat = G180gat & G177gat & G174gat & G171gat & G168gat & G165gat & G162gat & G154gat & G159gat;
  assign G203gat = ~G199gat;
  assign G213gat = ~G199gat;
  assign G224gat = G203gat ^ G154gat;
  assign G227gat = G203gat ^ G159gat;
  assign G230gat = G203gat ^ G162gat;
  assign G233gat = G203gat ^ G165gat;
  assign G236gat = G203gat ^ G168gat;
  assign G239gat = G203gat ^ G171gat;
  assign G242gat = ~G1gat | ~G213gat;
  assign G243gat = G203gat ^ G174gat;
  assign G246gat = ~G213gat | ~G11gat;
  assign G247gat = G203gat ^ G177gat;
  assign G250gat = ~G213gat | ~G24gat;
  assign G251gat = G203gat ^ G180gat;
  assign G254gat = ~G213gat | ~G37gat;
  assign G255gat = ~G213gat | ~G50gat;
  assign G256gat = ~G213gat | ~G63gat;
  assign G257gat = ~G213gat | ~G76gat;
  assign G258gat = ~G213gat | ~G89gat;
  assign G259gat = ~G213gat | ~G102gat;
  assign G260gat = ~G224gat | ~G157gat;
  assign G263gat = ~G224gat | ~G158gat;
  assign G264gat = ~G227gat | ~G183gat;
  assign G267gat = ~G230gat | ~G185gat;
  assign G270gat = ~G233gat | ~G187gat;
  assign G273gat = ~G236gat | ~G189gat;
  assign G276gat = ~G239gat | ~G191gat;
  assign G279gat = ~G243gat | ~G193gat;
  assign G282gat = ~G247gat | ~G195gat;
  assign G285gat = ~G251gat | ~G197gat;
  assign G288gat = ~G227gat | ~G184gat;
  assign G289gat = ~G230gat | ~G186gat;
  assign G290gat = ~G233gat | ~G188gat;
  assign G291gat = ~G236gat | ~G190gat;
  assign G292gat = ~G239gat | ~G192gat;
  assign G293gat = ~G243gat | ~G194gat;
  assign G294gat = ~G247gat | ~G196gat;
  assign G295gat = ~G251gat | ~G198gat;
  assign G296gat = G285gat & G282gat & G279gat & G276gat & G273gat & G270gat & G267gat & G260gat & G264gat;
  assign G300gat = ~G263gat;
  assign G301gat = ~G288gat;
  assign G302gat = ~G289gat;
  assign G303gat = ~G290gat;
  assign G304gat = ~G291gat;
  assign G305gat = ~G292gat;
  assign G306gat = ~G293gat;
  assign G307gat = ~G294gat;
  assign G308gat = ~G295gat;
  assign G309gat = ~G296gat;
  assign G319gat = ~G296gat;
  assign G330gat = G309gat ^ G260gat;
  assign G331gat = G309gat ^ G264gat;
  assign G332gat = G309gat ^ G267gat;
  assign G333gat = G309gat ^ G270gat;
  assign G334gat = ~G8gat | ~G319gat;
  assign G335gat = G309gat ^ G273gat;
  assign G336gat = ~G319gat | ~G21gat;
  assign G337gat = G309gat ^ G276gat;
  assign G338gat = ~G319gat | ~G34gat;
  assign G339gat = G309gat ^ G279gat;
  assign G340gat = ~G319gat | ~G47gat;
  assign G341gat = G309gat ^ G282gat;
  assign G342gat = ~G319gat | ~G60gat;
  assign G343gat = G309gat ^ G285gat;
  assign G344gat = ~G319gat | ~G73gat;
  assign G345gat = ~G319gat | ~G86gat;
  assign G346gat = ~G319gat | ~G99gat;
  assign G347gat = ~G319gat | ~G112gat;
  assign G348gat = ~G330gat | ~G300gat;
  assign G349gat = ~G331gat | ~G301gat;
  assign G350gat = ~G332gat | ~G302gat;
  assign G351gat = ~G333gat | ~G303gat;
  assign G352gat = ~G335gat | ~G304gat;
  assign G353gat = ~G337gat | ~G305gat;
  assign G354gat = ~G339gat | ~G306gat;
  assign G355gat = ~G341gat | ~G307gat;
  assign G356gat = ~G343gat | ~G308gat;
  assign G357gat = G356gat & G355gat & G354gat & G353gat & G352gat & G351gat & G350gat & G348gat & G349gat;
  assign G360gat = ~G357gat;
  assign G371gat = ~G14gat | ~G360gat;
  assign G372gat = ~G360gat | ~G27gat;
  assign G373gat = ~G360gat | ~G40gat;
  assign G374gat = ~G360gat | ~G53gat;
  assign G375gat = ~G360gat | ~G66gat;
  assign G376gat = ~G360gat | ~G79gat;
  assign G377gat = ~G360gat | ~G92gat;
  assign G378gat = ~G360gat | ~G105gat;
  assign G379gat = ~G360gat | ~G115gat;
  assign G380gat = ~G334gat | ~G371gat | ~G4gat | ~G242gat;
  assign G381gat = ~G372gat | ~G17gat | ~G246gat | ~G336gat;
  assign G386gat = ~G373gat | ~G30gat | ~G250gat | ~G338gat;
  assign G393gat = ~G374gat | ~G43gat | ~G254gat | ~G340gat;
  assign G399gat = ~G375gat | ~G56gat | ~G255gat | ~G342gat;
  assign G404gat = ~G376gat | ~G69gat | ~G256gat | ~G344gat;
  assign G407gat = ~G377gat | ~G82gat | ~G257gat | ~G345gat;
  assign G411gat = ~G378gat | ~G95gat | ~G258gat | ~G346gat;
  assign G414gat = ~G379gat | ~G108gat | ~G259gat | ~G347gat;
  assign G415gat = ~G380gat;
  assign G416gat = G414gat & G411gat & G407gat & G404gat & G399gat & G393gat & G381gat & G386gat;
  assign G417gat = ~G393gat;
  assign G418gat = ~G404gat;
  assign G419gat = ~G407gat;
  assign G420gat = ~G411gat;
  assign G422gat = ~G386gat | ~G417gat;
  assign G425gat = ~G418gat | ~G399gat | ~G386gat | ~G393gat;
  assign G428gat = ~G419gat | ~G399gat | ~G393gat;
  assign G429gat = ~G407gat | ~G420gat | ~G386gat | ~G393gat;
endmodule


