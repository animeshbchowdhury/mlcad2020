// Benchmark "b11_C" written by ABC on Thu Oct  3 17:49:21 2019

module top ( 
    X_IN_5_, X_IN_4_, X_IN_3_, X_IN_2_, X_IN_1_, X_IN_0_, STBI,
    STATO_REG_0__SCAN_IN, STATO_REG_1__SCAN_IN, STATO_REG_2__SCAN_IN,
    STATO_REG_3__SCAN_IN, X_OUT_REG_0__SCAN_IN, X_OUT_REG_1__SCAN_IN,
    X_OUT_REG_2__SCAN_IN, X_OUT_REG_3__SCAN_IN, X_OUT_REG_4__SCAN_IN,
    X_OUT_REG_5__SCAN_IN, CONT1_REG_0__SCAN_IN, CONT1_REG_1__SCAN_IN,
    R_IN_REG_5__SCAN_IN, R_IN_REG_4__SCAN_IN, R_IN_REG_3__SCAN_IN,
    R_IN_REG_2__SCAN_IN, R_IN_REG_1__SCAN_IN, R_IN_REG_0__SCAN_IN,
    CONT_REG_5__SCAN_IN, CONT_REG_4__SCAN_IN, CONT_REG_3__SCAN_IN,
    CONT_REG_2__SCAN_IN, CONT_REG_1__SCAN_IN, CONT_REG_0__SCAN_IN,
    CONT1_REG_8__SCAN_IN, CONT1_REG_7__SCAN_IN, CONT1_REG_6__SCAN_IN,
    CONT1_REG_5__SCAN_IN, CONT1_REG_4__SCAN_IN, CONT1_REG_3__SCAN_IN,
    CONT1_REG_2__SCAN_IN,
    U404, U405, U406, U407, U408, U409, U384, U383, U382, U381, U380, U379,
    U378, U377, U376, U375, U374, U373, U372, U371, U370, U369, U368, U367,
    U366, U365, U364, U360, U361, U362, U363  );
  input  X_IN_5_, X_IN_4_, X_IN_3_, X_IN_2_, X_IN_1_, X_IN_0_, STBI,
    STATO_REG_0__SCAN_IN, STATO_REG_1__SCAN_IN, STATO_REG_2__SCAN_IN,
    STATO_REG_3__SCAN_IN, X_OUT_REG_0__SCAN_IN, X_OUT_REG_1__SCAN_IN,
    X_OUT_REG_2__SCAN_IN, X_OUT_REG_3__SCAN_IN, X_OUT_REG_4__SCAN_IN,
    X_OUT_REG_5__SCAN_IN, CONT1_REG_0__SCAN_IN, CONT1_REG_1__SCAN_IN,
    R_IN_REG_5__SCAN_IN, R_IN_REG_4__SCAN_IN, R_IN_REG_3__SCAN_IN,
    R_IN_REG_2__SCAN_IN, R_IN_REG_1__SCAN_IN, R_IN_REG_0__SCAN_IN,
    CONT_REG_5__SCAN_IN, CONT_REG_4__SCAN_IN, CONT_REG_3__SCAN_IN,
    CONT_REG_2__SCAN_IN, CONT_REG_1__SCAN_IN, CONT_REG_0__SCAN_IN,
    CONT1_REG_8__SCAN_IN, CONT1_REG_7__SCAN_IN, CONT1_REG_6__SCAN_IN,
    CONT1_REG_5__SCAN_IN, CONT1_REG_4__SCAN_IN, CONT1_REG_3__SCAN_IN,
    CONT1_REG_2__SCAN_IN;
  output U404, U405, U406, U407, U408, U409, U384, U383, U382, U381, U380,
    U379, U378, U377, U376, U375, U374, U373, U372, U371, U370, U369, U368,
    U367, U366, U365, U364, U360, U361, U362, U363;
  wire new_n76_, new_n77_, new_n78_, new_n79_, new_n80_, new_n81_, new_n83_,
    new_n84_, new_n85_, new_n87_, new_n88_, new_n89_, new_n91_, new_n92_,
    new_n93_, new_n95_, new_n96_, new_n97_, new_n99_, new_n100_, new_n101_,
    new_n103_, new_n104_, new_n105_, new_n106_, new_n107_, new_n108_,
    new_n109_, new_n110_, new_n111_, new_n112_, new_n113_, new_n114_,
    new_n115_, new_n116_, new_n117_, new_n118_, new_n119_, new_n120_,
    new_n121_, new_n122_, new_n123_, new_n124_, new_n125_, new_n126_,
    new_n127_, new_n128_, new_n129_, new_n130_, new_n131_, new_n132_,
    new_n133_, new_n134_, new_n135_, new_n136_, new_n137_, new_n138_,
    new_n139_, new_n140_, new_n141_, new_n142_, new_n143_, new_n144_,
    new_n145_, new_n146_, new_n147_, new_n148_, new_n149_, new_n150_,
    new_n151_, new_n152_, new_n153_, new_n154_, new_n155_, new_n156_,
    new_n157_, new_n158_, new_n159_, new_n160_, new_n161_, new_n162_,
    new_n163_, new_n164_, new_n165_, new_n166_, new_n167_, new_n168_,
    new_n169_, new_n170_, new_n171_, new_n172_, new_n173_, new_n174_,
    new_n175_, new_n177_, new_n178_, new_n179_, new_n181_, new_n182_,
    new_n183_, new_n185_, new_n186_, new_n187_, new_n189_, new_n190_,
    new_n191_, new_n193_, new_n194_, new_n195_, new_n197_, new_n198_,
    new_n199_, new_n200_, new_n201_, new_n202_, new_n203_, new_n204_,
    new_n205_, new_n206_, new_n207_, new_n208_, new_n209_, new_n210_,
    new_n211_, new_n212_, new_n213_, new_n214_, new_n215_, new_n216_,
    new_n217_, new_n218_, new_n219_, new_n220_, new_n221_, new_n222_,
    new_n223_, new_n224_, new_n225_, new_n226_, new_n227_, new_n228_,
    new_n229_, new_n230_, new_n231_, new_n232_, new_n233_, new_n234_,
    new_n235_, new_n236_, new_n237_, new_n238_, new_n239_, new_n240_,
    new_n241_, new_n242_, new_n243_, new_n244_, new_n245_, new_n246_,
    new_n247_, new_n248_, new_n249_, new_n250_, new_n251_, new_n252_,
    new_n253_, new_n254_, new_n255_, new_n256_, new_n257_, new_n258_,
    new_n259_, new_n260_, new_n261_, new_n262_, new_n263_, new_n264_,
    new_n265_, new_n266_, new_n267_, new_n268_, new_n269_, new_n270_,
    new_n271_, new_n272_, new_n273_, new_n274_, new_n275_, new_n276_,
    new_n277_, new_n278_, new_n279_, new_n280_, new_n281_, new_n282_,
    new_n283_, new_n284_, new_n285_, new_n286_, new_n287_, new_n288_,
    new_n289_, new_n290_, new_n291_, new_n292_, new_n293_, new_n294_,
    new_n295_, new_n296_, new_n297_, new_n298_, new_n299_, new_n300_,
    new_n301_, new_n302_, new_n303_, new_n304_, new_n305_, new_n306_,
    new_n307_, new_n308_, new_n309_, new_n310_, new_n311_, new_n312_,
    new_n313_, new_n314_, new_n315_, new_n316_, new_n317_, new_n318_,
    new_n319_, new_n320_, new_n321_, new_n322_, new_n323_, new_n324_,
    new_n325_, new_n326_, new_n327_, new_n328_, new_n329_, new_n330_,
    new_n331_, new_n332_, new_n333_, new_n334_, new_n335_, new_n336_,
    new_n337_, new_n338_, new_n339_, new_n340_, new_n341_, new_n342_,
    new_n343_, new_n344_, new_n345_, new_n346_, new_n347_, new_n348_,
    new_n349_, new_n350_, new_n351_, new_n352_, new_n353_, new_n354_,
    new_n355_, new_n356_, new_n357_, new_n358_, new_n359_, new_n360_,
    new_n361_, new_n362_, new_n363_, new_n364_, new_n365_, new_n366_,
    new_n367_, new_n368_, new_n370_, new_n371_, new_n372_, new_n373_,
    new_n374_, new_n375_, new_n376_, new_n377_, new_n378_, new_n379_,
    new_n380_, new_n381_, new_n382_, new_n383_, new_n384_, new_n385_,
    new_n386_, new_n388_, new_n389_, new_n390_, new_n391_, new_n392_,
    new_n393_, new_n394_, new_n395_, new_n396_, new_n397_, new_n398_,
    new_n399_, new_n400_, new_n401_, new_n402_, new_n403_, new_n404_,
    new_n405_, new_n406_, new_n407_, new_n408_, new_n409_, new_n410_,
    new_n412_, new_n413_, new_n414_, new_n415_, new_n416_, new_n417_,
    new_n418_, new_n419_, new_n420_, new_n421_, new_n422_, new_n423_,
    new_n424_, new_n425_, new_n426_, new_n427_, new_n428_, new_n429_,
    new_n430_, new_n431_, new_n432_, new_n433_, new_n434_, new_n435_,
    new_n436_, new_n437_, new_n438_, new_n440_, new_n441_, new_n442_,
    new_n443_, new_n444_, new_n445_, new_n446_, new_n447_, new_n448_,
    new_n449_, new_n450_, new_n451_, new_n452_, new_n453_, new_n454_,
    new_n455_, new_n456_, new_n457_, new_n458_, new_n459_, new_n460_,
    new_n461_, new_n463_, new_n464_, new_n465_, new_n466_, new_n467_,
    new_n468_, new_n469_, new_n470_, new_n471_, new_n472_, new_n473_,
    new_n474_, new_n475_, new_n476_, new_n477_, new_n478_, new_n479_,
    new_n480_, new_n481_, new_n482_, new_n483_, new_n484_, new_n485_,
    new_n486_, new_n488_, new_n489_, new_n490_, new_n491_, new_n492_,
    new_n493_, new_n494_, new_n495_, new_n496_, new_n497_, new_n498_,
    new_n499_, new_n500_, new_n501_, new_n502_, new_n503_, new_n504_,
    new_n505_, new_n506_, new_n507_, new_n508_, new_n509_, new_n510_,
    new_n512_, new_n513_, new_n514_, new_n515_, new_n516_, new_n517_,
    new_n518_, new_n519_, new_n520_, new_n521_, new_n522_, new_n523_,
    new_n524_, new_n525_, new_n526_, new_n527_, new_n528_, new_n529_,
    new_n530_, new_n531_, new_n532_, new_n534_, new_n535_, new_n536_,
    new_n537_, new_n538_, new_n539_, new_n540_, new_n541_, new_n542_,
    new_n543_, new_n544_, new_n545_, new_n546_, new_n547_, new_n548_,
    new_n549_, new_n550_, new_n551_, new_n552_, new_n553_, new_n555_,
    new_n556_, new_n557_, new_n558_, new_n559_, new_n560_, new_n561_,
    new_n562_, new_n563_, new_n565_, new_n566_, new_n567_, new_n568_,
    new_n569_, new_n571_, new_n572_, new_n573_, new_n574_, new_n576_,
    new_n577_, new_n578_, new_n579_, new_n581_, new_n582_, new_n583_,
    new_n584_, new_n586_, new_n587_, new_n588_, new_n589_, new_n591_,
    new_n592_, new_n594_, new_n595_, new_n597_, new_n598_, new_n599_,
    new_n600_, new_n601_, new_n602_, new_n603_, new_n604_, new_n605_,
    new_n606_, new_n607_, new_n608_, new_n610_, new_n611_, new_n612_,
    new_n613_, new_n614_, new_n615_, new_n616_, new_n617_;
  inv01  g000(.A(STATO_REG_3__SCAN_IN), .Y(new_n76_));
  nor02  g001(.A0(STATO_REG_2__SCAN_IN), .A1(STATO_REG_1__SCAN_IN), .Y(new_n77_));
  and02  g002(.A0(new_n77_), .A1(new_n76_), .Y(new_n78_));
  and02  g003(.A0(new_n78_), .A1(X_IN_5_), .Y(new_n79_));
  inv01  g004(.A(R_IN_REG_5__SCAN_IN), .Y(new_n80_));
  nor02  g005(.A0(new_n78_), .A1(new_n80_), .Y(new_n81_));
  or02   g006(.A0(new_n81_), .A1(new_n79_), .Y(U404));
  and02  g007(.A0(new_n78_), .A1(X_IN_4_), .Y(new_n83_));
  inv01  g008(.A(R_IN_REG_4__SCAN_IN), .Y(new_n84_));
  nor02  g009(.A0(new_n78_), .A1(new_n84_), .Y(new_n85_));
  or02   g010(.A0(new_n85_), .A1(new_n83_), .Y(U405));
  and02  g011(.A0(new_n78_), .A1(X_IN_3_), .Y(new_n87_));
  inv01  g012(.A(R_IN_REG_3__SCAN_IN), .Y(new_n88_));
  nor02  g013(.A0(new_n78_), .A1(new_n88_), .Y(new_n89_));
  or02   g014(.A0(new_n89_), .A1(new_n87_), .Y(U406));
  and02  g015(.A0(new_n78_), .A1(X_IN_2_), .Y(new_n91_));
  inv01  g016(.A(R_IN_REG_2__SCAN_IN), .Y(new_n92_));
  nor02  g017(.A0(new_n78_), .A1(new_n92_), .Y(new_n93_));
  or02   g018(.A0(new_n93_), .A1(new_n91_), .Y(U407));
  and02  g019(.A0(new_n78_), .A1(X_IN_1_), .Y(new_n95_));
  inv01  g020(.A(R_IN_REG_1__SCAN_IN), .Y(new_n96_));
  nor02  g021(.A0(new_n78_), .A1(new_n96_), .Y(new_n97_));
  or02   g022(.A0(new_n97_), .A1(new_n95_), .Y(U408));
  and02  g023(.A0(new_n78_), .A1(X_IN_0_), .Y(new_n99_));
  inv01  g024(.A(R_IN_REG_0__SCAN_IN), .Y(new_n100_));
  nor02  g025(.A0(new_n78_), .A1(new_n100_), .Y(new_n101_));
  or02   g026(.A0(new_n101_), .A1(new_n99_), .Y(U409));
  inv01  g027(.A(STATO_REG_0__SCAN_IN), .Y(new_n103_));
  inv01  g028(.A(STATO_REG_2__SCAN_IN), .Y(new_n104_));
  and02  g029(.A0(new_n104_), .A1(STATO_REG_1__SCAN_IN), .Y(new_n105_));
  and02  g030(.A0(new_n105_), .A1(new_n103_), .Y(new_n106_));
  and02  g031(.A0(R_IN_REG_4__SCAN_IN), .A1(new_n80_), .Y(new_n107_));
  and02  g032(.A0(new_n88_), .A1(R_IN_REG_5__SCAN_IN), .Y(new_n108_));
  nor02  g033(.A0(new_n108_), .A1(new_n107_), .Y(new_n109_));
  and02  g034(.A0(new_n100_), .A1(R_IN_REG_2__SCAN_IN), .Y(new_n110_));
  and02  g035(.A0(R_IN_REG_0__SCAN_IN), .A1(new_n84_), .Y(new_n111_));
  nor02  g036(.A0(new_n111_), .A1(new_n110_), .Y(new_n112_));
  and02  g037(.A0(R_IN_REG_1__SCAN_IN), .A1(new_n92_), .Y(new_n113_));
  and02  g038(.A0(new_n96_), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n114_));
  nor02  g039(.A0(new_n114_), .A1(new_n113_), .Y(new_n115_));
  and02  g040(.A0(new_n115_), .A1(new_n112_), .Y(new_n116_));
  and02  g041(.A0(new_n116_), .A1(new_n109_), .Y(new_n117_));
  and02  g042(.A0(new_n117_), .A1(new_n106_), .Y(new_n118_));
  and02  g043(.A0(new_n77_), .A1(new_n103_), .Y(new_n119_));
  and02  g044(.A0(new_n119_), .A1(new_n76_), .Y(new_n120_));
  nor02  g045(.A0(new_n120_), .A1(new_n118_), .Y(new_n121_));
  and02  g046(.A0(new_n121_), .A1(CONT_REG_5__SCAN_IN), .Y(new_n122_));
  and02  g047(.A0(CONT_REG_0__SCAN_IN), .A1(CONT_REG_1__SCAN_IN), .Y(new_n123_));
  and02  g048(.A0(new_n123_), .A1(CONT_REG_2__SCAN_IN), .Y(new_n124_));
  and02  g049(.A0(new_n124_), .A1(CONT_REG_3__SCAN_IN), .Y(new_n125_));
  and02  g050(.A0(new_n125_), .A1(CONT_REG_4__SCAN_IN), .Y(new_n126_));
  xor2   g051(.A0(new_n126_), .A1(CONT_REG_5__SCAN_IN), .Y(new_n127_));
  and02  g052(.A0(new_n117_), .A1(CONT_REG_5__SCAN_IN), .Y(new_n128_));
  nor02  g053(.A0(new_n117_), .A1(new_n80_), .Y(new_n129_));
  inv01  g054(.A(new_n129_), .Y(new_n130_));
  and02  g055(.A0(new_n130_), .A1(new_n128_), .Y(new_n131_));
  nor02  g056(.A0(new_n117_), .A1(new_n96_), .Y(new_n132_));
  inv01  g057(.A(CONT_REG_1__SCAN_IN), .Y(new_n133_));
  and02  g058(.A0(new_n117_), .A1(new_n133_), .Y(new_n134_));
  nor02  g059(.A0(new_n134_), .A1(new_n132_), .Y(new_n135_));
  inv01  g060(.A(new_n135_), .Y(new_n136_));
  nor02  g061(.A0(new_n117_), .A1(R_IN_REG_0__SCAN_IN), .Y(new_n137_));
  and02  g062(.A0(new_n117_), .A1(CONT_REG_0__SCAN_IN), .Y(new_n138_));
  nor02  g063(.A0(new_n138_), .A1(new_n137_), .Y(new_n139_));
  and02  g064(.A0(new_n139_), .A1(new_n136_), .Y(new_n140_));
  inv01  g065(.A(new_n140_), .Y(new_n141_));
  and02  g066(.A0(new_n134_), .A1(new_n132_), .Y(new_n142_));
  and02  g067(.A0(new_n117_), .A1(CONT_REG_2__SCAN_IN), .Y(new_n143_));
  inv01  g068(.A(new_n143_), .Y(new_n144_));
  nor02  g069(.A0(new_n117_), .A1(new_n92_), .Y(new_n145_));
  and02  g070(.A0(new_n145_), .A1(new_n144_), .Y(new_n146_));
  nor02  g071(.A0(new_n146_), .A1(new_n142_), .Y(new_n147_));
  and02  g072(.A0(new_n147_), .A1(new_n141_), .Y(new_n148_));
  nor02  g073(.A0(new_n145_), .A1(new_n144_), .Y(new_n149_));
  nor02  g074(.A0(new_n117_), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n150_));
  inv01  g075(.A(CONT_REG_3__SCAN_IN), .Y(new_n151_));
  and02  g076(.A0(new_n117_), .A1(new_n151_), .Y(new_n152_));
  inv01  g077(.A(new_n152_), .Y(new_n153_));
  and02  g078(.A0(new_n153_), .A1(new_n150_), .Y(new_n154_));
  nor02  g079(.A0(new_n154_), .A1(new_n149_), .Y(new_n155_));
  inv01  g080(.A(new_n155_), .Y(new_n156_));
  nor02  g081(.A0(new_n156_), .A1(new_n148_), .Y(new_n157_));
  nor02  g082(.A0(new_n153_), .A1(new_n150_), .Y(new_n158_));
  nor02  g083(.A0(new_n117_), .A1(R_IN_REG_4__SCAN_IN), .Y(new_n159_));
  inv01  g084(.A(new_n159_), .Y(new_n160_));
  inv01  g085(.A(CONT_REG_4__SCAN_IN), .Y(new_n161_));
  and02  g086(.A0(new_n117_), .A1(new_n161_), .Y(new_n162_));
  and02  g087(.A0(new_n162_), .A1(new_n160_), .Y(new_n163_));
  nor02  g088(.A0(new_n163_), .A1(new_n158_), .Y(new_n164_));
  inv01  g089(.A(new_n164_), .Y(new_n165_));
  nor02  g090(.A0(new_n165_), .A1(new_n157_), .Y(new_n166_));
  nor02  g091(.A0(new_n162_), .A1(new_n160_), .Y(new_n167_));
  nor02  g092(.A0(new_n167_), .A1(new_n166_), .Y(new_n168_));
  nor02  g093(.A0(new_n130_), .A1(new_n128_), .Y(new_n169_));
  nor02  g094(.A0(new_n169_), .A1(new_n168_), .Y(new_n170_));
  nor02  g095(.A0(new_n170_), .A1(new_n131_), .Y(new_n171_));
  inv01  g096(.A(STATO_REG_1__SCAN_IN), .Y(new_n172_));
  nor02  g097(.A0(new_n121_), .A1(new_n172_), .Y(new_n173_));
  and02  g098(.A0(new_n173_), .A1(new_n171_), .Y(new_n174_));
  and02  g099(.A0(new_n174_), .A1(new_n127_), .Y(new_n175_));
  or02   g100(.A0(new_n175_), .A1(new_n122_), .Y(U384));
  and02  g101(.A0(new_n121_), .A1(CONT_REG_4__SCAN_IN), .Y(new_n177_));
  xor2   g102(.A0(new_n125_), .A1(CONT_REG_4__SCAN_IN), .Y(new_n178_));
  and02  g103(.A0(new_n178_), .A1(new_n174_), .Y(new_n179_));
  or02   g104(.A0(new_n179_), .A1(new_n177_), .Y(U383));
  and02  g105(.A0(new_n121_), .A1(CONT_REG_3__SCAN_IN), .Y(new_n181_));
  xor2   g106(.A0(new_n124_), .A1(CONT_REG_3__SCAN_IN), .Y(new_n182_));
  and02  g107(.A0(new_n182_), .A1(new_n174_), .Y(new_n183_));
  or02   g108(.A0(new_n183_), .A1(new_n181_), .Y(U382));
  and02  g109(.A0(new_n121_), .A1(CONT_REG_2__SCAN_IN), .Y(new_n185_));
  xor2   g110(.A0(new_n123_), .A1(CONT_REG_2__SCAN_IN), .Y(new_n186_));
  and02  g111(.A0(new_n186_), .A1(new_n174_), .Y(new_n187_));
  or02   g112(.A0(new_n187_), .A1(new_n185_), .Y(U381));
  and02  g113(.A0(new_n121_), .A1(CONT_REG_1__SCAN_IN), .Y(new_n189_));
  xor2   g114(.A0(CONT_REG_0__SCAN_IN), .A1(CONT_REG_1__SCAN_IN), .Y(new_n190_));
  and02  g115(.A0(new_n190_), .A1(new_n174_), .Y(new_n191_));
  or02   g116(.A0(new_n191_), .A1(new_n189_), .Y(U380));
  and02  g117(.A0(new_n121_), .A1(CONT_REG_0__SCAN_IN), .Y(new_n193_));
  inv01  g118(.A(CONT_REG_0__SCAN_IN), .Y(new_n194_));
  and02  g119(.A0(new_n174_), .A1(new_n194_), .Y(new_n195_));
  or02   g120(.A0(new_n195_), .A1(new_n193_), .Y(U379));
  xor2   g121(.A0(CONT1_REG_8__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n197_));
  inv01  g122(.A(CONT1_REG_7__SCAN_IN), .Y(new_n198_));
  inv01  g123(.A(CONT1_REG_5__SCAN_IN), .Y(new_n199_));
  xnor2  g124(.A0(R_IN_REG_2__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n200_));
  xor2   g125(.A0(new_n200_), .A1(new_n88_), .Y(new_n201_));
  and02  g126(.A0(new_n201_), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n202_));
  nor02  g127(.A0(new_n201_), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n203_));
  inv01  g128(.A(new_n203_), .Y(new_n204_));
  xnor2  g129(.A0(R_IN_REG_2__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n205_));
  and02  g130(.A0(new_n205_), .A1(CONT1_REG_3__SCAN_IN), .Y(new_n206_));
  inv01  g131(.A(CONT1_REG_3__SCAN_IN), .Y(new_n207_));
  xor2   g132(.A0(R_IN_REG_2__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n208_));
  and02  g133(.A0(new_n208_), .A1(new_n207_), .Y(new_n209_));
  or02   g134(.A0(R_IN_REG_2__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n210_));
  and02  g135(.A0(new_n210_), .A1(CONT1_REG_2__SCAN_IN), .Y(new_n211_));
  nor02  g136(.A0(new_n210_), .A1(CONT1_REG_2__SCAN_IN), .Y(new_n212_));
  inv01  g137(.A(new_n212_), .Y(new_n213_));
  nand02 g138(.A0(new_n208_), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n214_));
  or02   g139(.A0(new_n208_), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n215_));
  nand02 g140(.A0(new_n215_), .A1(new_n88_), .Y(new_n216_));
  and02  g141(.A0(new_n216_), .A1(new_n214_), .Y(new_n217_));
  or02   g142(.A0(new_n217_), .A1(new_n201_), .Y(new_n218_));
  nand02 g143(.A0(new_n217_), .A1(new_n201_), .Y(new_n219_));
  nand02 g144(.A0(new_n219_), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n220_));
  nand02 g145(.A0(new_n220_), .A1(new_n218_), .Y(new_n221_));
  and02  g146(.A0(new_n221_), .A1(new_n213_), .Y(new_n222_));
  nor02  g147(.A0(new_n222_), .A1(new_n211_), .Y(new_n223_));
  nor02  g148(.A0(new_n223_), .A1(new_n209_), .Y(new_n224_));
  or02   g149(.A0(new_n224_), .A1(new_n206_), .Y(new_n225_));
  and02  g150(.A0(new_n225_), .A1(new_n204_), .Y(new_n226_));
  nor02  g151(.A0(new_n226_), .A1(new_n202_), .Y(new_n227_));
  or02   g152(.A0(new_n227_), .A1(new_n199_), .Y(new_n228_));
  and02  g153(.A0(new_n227_), .A1(new_n199_), .Y(new_n229_));
  or02   g154(.A0(new_n229_), .A1(new_n210_), .Y(new_n230_));
  nand02 g155(.A0(new_n230_), .A1(new_n228_), .Y(new_n231_));
  nand02 g156(.A0(new_n231_), .A1(CONT1_REG_6__SCAN_IN), .Y(new_n232_));
  or02   g157(.A0(new_n231_), .A1(CONT1_REG_6__SCAN_IN), .Y(new_n233_));
  nand02 g158(.A0(new_n233_), .A1(new_n88_), .Y(new_n234_));
  and02  g159(.A0(new_n234_), .A1(new_n232_), .Y(new_n235_));
  or02   g160(.A0(new_n235_), .A1(new_n198_), .Y(new_n236_));
  nand02 g161(.A0(new_n235_), .A1(new_n198_), .Y(new_n237_));
  nand02 g162(.A0(new_n237_), .A1(new_n88_), .Y(new_n238_));
  and02  g163(.A0(new_n238_), .A1(new_n236_), .Y(new_n239_));
  nand02 g164(.A0(new_n239_), .A1(new_n197_), .Y(new_n240_));
  or02   g165(.A0(new_n239_), .A1(new_n197_), .Y(new_n241_));
  nand02 g166(.A0(STATO_REG_2__SCAN_IN), .A1(STATO_REG_1__SCAN_IN), .Y(new_n242_));
  and02  g167(.A0(CONT1_REG_1__SCAN_IN), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n243_));
  nor02  g168(.A0(new_n243_), .A1(CONT1_REG_2__SCAN_IN), .Y(new_n244_));
  and02  g169(.A0(CONT1_REG_3__SCAN_IN), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n245_));
  inv01  g170(.A(new_n245_), .Y(new_n246_));
  nor02  g171(.A0(new_n246_), .A1(new_n244_), .Y(new_n247_));
  nor02  g172(.A0(CONT1_REG_6__SCAN_IN), .A1(CONT1_REG_7__SCAN_IN), .Y(new_n248_));
  and02  g173(.A0(new_n248_), .A1(new_n199_), .Y(new_n249_));
  inv01  g174(.A(new_n249_), .Y(new_n250_));
  nor02  g175(.A0(new_n250_), .A1(new_n247_), .Y(new_n251_));
  nor02  g176(.A0(new_n251_), .A1(CONT1_REG_8__SCAN_IN), .Y(new_n252_));
  nor02  g177(.A0(new_n252_), .A1(new_n103_), .Y(new_n253_));
  nor02  g178(.A0(new_n248_), .A1(CONT1_REG_8__SCAN_IN), .Y(new_n254_));
  nor02  g179(.A0(new_n254_), .A1(new_n172_), .Y(new_n255_));
  nor02  g180(.A0(new_n255_), .A1(new_n104_), .Y(new_n256_));
  inv01  g181(.A(new_n256_), .Y(new_n257_));
  nor02  g182(.A0(new_n257_), .A1(new_n253_), .Y(new_n258_));
  inv01  g183(.A(new_n258_), .Y(new_n259_));
  and02  g184(.A0(STATO_REG_1__SCAN_IN), .A1(STATO_REG_0__SCAN_IN), .Y(new_n260_));
  and02  g185(.A0(new_n117_), .A1(new_n105_), .Y(new_n261_));
  nor02  g186(.A0(new_n261_), .A1(new_n260_), .Y(new_n262_));
  and02  g187(.A0(new_n262_), .A1(new_n259_), .Y(new_n263_));
  nor02  g188(.A0(new_n263_), .A1(new_n242_), .Y(new_n264_));
  and02  g189(.A0(new_n264_), .A1(STATO_REG_0__SCAN_IN), .Y(new_n265_));
  and02  g190(.A0(new_n265_), .A1(new_n241_), .Y(new_n266_));
  and02  g191(.A0(new_n266_), .A1(new_n240_), .Y(new_n267_));
  and02  g192(.A0(CONT1_REG_5__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n268_));
  and02  g193(.A0(CONT1_REG_8__SCAN_IN), .A1(new_n76_), .Y(new_n269_));
  xor2   g194(.A0(new_n269_), .A1(new_n268_), .Y(new_n270_));
  and02  g195(.A0(CONT1_REG_6__SCAN_IN), .A1(new_n76_), .Y(new_n271_));
  inv01  g196(.A(new_n271_), .Y(new_n272_));
  inv01  g197(.A(new_n268_), .Y(new_n273_));
  and02  g198(.A0(CONT1_REG_6__SCAN_IN), .A1(new_n76_), .Y(new_n274_));
  nor02  g199(.A0(new_n274_), .A1(new_n273_), .Y(new_n275_));
  and02  g200(.A0(CONT1_REG_5__SCAN_IN), .A1(new_n76_), .Y(new_n276_));
  inv01  g201(.A(new_n276_), .Y(new_n277_));
  and02  g202(.A0(CONT1_REG_5__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n278_));
  and02  g203(.A0(CONT1_REG_3__SCAN_IN), .A1(new_n76_), .Y(new_n279_));
  and02  g204(.A0(new_n207_), .A1(STATO_REG_3__SCAN_IN), .Y(new_n280_));
  and02  g205(.A0(new_n280_), .A1(new_n279_), .Y(new_n281_));
  xnor2  g206(.A0(CONT1_REG_3__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n282_));
  and02  g207(.A0(CONT1_REG_1__SCAN_IN), .A1(new_n76_), .Y(new_n283_));
  inv01  g208(.A(CONT1_REG_1__SCAN_IN), .Y(new_n284_));
  and02  g209(.A0(new_n284_), .A1(STATO_REG_3__SCAN_IN), .Y(new_n285_));
  and02  g210(.A0(new_n285_), .A1(new_n283_), .Y(new_n286_));
  xnor2  g211(.A0(CONT1_REG_1__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n287_));
  and02  g212(.A0(CONT1_REG_0__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n288_));
  nor02  g213(.A0(new_n288_), .A1(new_n287_), .Y(new_n289_));
  nor02  g214(.A0(new_n289_), .A1(new_n286_), .Y(new_n290_));
  and02  g215(.A0(CONT1_REG_2__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n291_));
  nor02  g216(.A0(new_n291_), .A1(new_n290_), .Y(new_n292_));
  and02  g217(.A0(CONT1_REG_2__SCAN_IN), .A1(new_n76_), .Y(new_n293_));
  nor02  g218(.A0(new_n293_), .A1(new_n292_), .Y(new_n294_));
  nor02  g219(.A0(new_n294_), .A1(new_n282_), .Y(new_n295_));
  nor02  g220(.A0(new_n295_), .A1(new_n281_), .Y(new_n296_));
  inv01  g221(.A(CONT1_REG_4__SCAN_IN), .Y(new_n297_));
  and02  g222(.A0(new_n297_), .A1(STATO_REG_3__SCAN_IN), .Y(new_n298_));
  inv01  g223(.A(new_n298_), .Y(new_n299_));
  nor02  g224(.A0(new_n299_), .A1(new_n296_), .Y(new_n300_));
  inv01  g225(.A(new_n300_), .Y(new_n301_));
  and02  g226(.A0(CONT1_REG_4__SCAN_IN), .A1(new_n76_), .Y(new_n302_));
  inv01  g227(.A(new_n302_), .Y(new_n303_));
  and02  g228(.A0(new_n299_), .A1(new_n296_), .Y(new_n304_));
  or02   g229(.A0(new_n304_), .A1(new_n303_), .Y(new_n305_));
  and02  g230(.A0(new_n305_), .A1(new_n301_), .Y(new_n306_));
  or02   g231(.A0(new_n306_), .A1(new_n278_), .Y(new_n307_));
  and02  g232(.A0(new_n307_), .A1(new_n277_), .Y(new_n308_));
  or02   g233(.A0(new_n308_), .A1(new_n275_), .Y(new_n309_));
  and02  g234(.A0(new_n309_), .A1(new_n272_), .Y(new_n310_));
  or02   g235(.A0(new_n310_), .A1(new_n268_), .Y(new_n311_));
  or02   g236(.A0(new_n198_), .A1(STATO_REG_3__SCAN_IN), .Y(new_n312_));
  and02  g237(.A0(new_n312_), .A1(new_n311_), .Y(new_n313_));
  xor2   g238(.A0(new_n313_), .A1(new_n270_), .Y(new_n314_));
  nor02  g239(.A0(new_n263_), .A1(STATO_REG_1__SCAN_IN), .Y(new_n315_));
  and02  g240(.A0(new_n315_), .A1(STATO_REG_0__SCAN_IN), .Y(new_n316_));
  and02  g241(.A0(new_n316_), .A1(new_n314_), .Y(new_n317_));
  xnor2  g242(.A0(CONT1_REG_8__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n318_));
  xor2   g243(.A0(CONT1_REG_6__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n319_));
  xor2   g244(.A0(CONT1_REG_5__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n320_));
  nor02  g245(.A0(new_n320_), .A1(new_n80_), .Y(new_n321_));
  and02  g246(.A0(new_n320_), .A1(new_n80_), .Y(new_n322_));
  xor2   g247(.A0(CONT1_REG_4__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n323_));
  nor02  g248(.A0(new_n323_), .A1(new_n84_), .Y(new_n324_));
  and02  g249(.A0(new_n323_), .A1(new_n84_), .Y(new_n325_));
  xor2   g250(.A0(CONT1_REG_3__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n326_));
  nor02  g251(.A0(new_n326_), .A1(new_n88_), .Y(new_n327_));
  and02  g252(.A0(new_n326_), .A1(new_n88_), .Y(new_n328_));
  xor2   g253(.A0(CONT1_REG_2__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n329_));
  nor02  g254(.A0(new_n329_), .A1(new_n92_), .Y(new_n330_));
  and02  g255(.A0(new_n329_), .A1(new_n92_), .Y(new_n331_));
  and02  g256(.A0(R_IN_REG_1__SCAN_IN), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n332_));
  and02  g257(.A0(new_n96_), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n333_));
  xor2   g258(.A0(R_IN_REG_1__SCAN_IN), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n334_));
  nor02  g259(.A0(new_n334_), .A1(new_n100_), .Y(new_n335_));
  and02  g260(.A0(new_n334_), .A1(new_n100_), .Y(new_n336_));
  nor02  g261(.A0(new_n336_), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n337_));
  nor02  g262(.A0(new_n337_), .A1(new_n335_), .Y(new_n338_));
  nor02  g263(.A0(new_n338_), .A1(new_n333_), .Y(new_n339_));
  nor02  g264(.A0(new_n339_), .A1(new_n332_), .Y(new_n340_));
  nor02  g265(.A0(new_n340_), .A1(new_n331_), .Y(new_n341_));
  nor02  g266(.A0(new_n341_), .A1(new_n330_), .Y(new_n342_));
  nor02  g267(.A0(new_n342_), .A1(new_n328_), .Y(new_n343_));
  nor02  g268(.A0(new_n343_), .A1(new_n327_), .Y(new_n344_));
  nor02  g269(.A0(new_n344_), .A1(new_n325_), .Y(new_n345_));
  nor02  g270(.A0(new_n345_), .A1(new_n324_), .Y(new_n346_));
  nor02  g271(.A0(new_n346_), .A1(new_n322_), .Y(new_n347_));
  nor02  g272(.A0(new_n347_), .A1(new_n321_), .Y(new_n348_));
  nor02  g273(.A0(new_n348_), .A1(new_n319_), .Y(new_n349_));
  inv01  g274(.A(new_n349_), .Y(new_n350_));
  xor2   g275(.A0(CONT1_REG_7__SCAN_IN), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n351_));
  nor02  g276(.A0(new_n351_), .A1(new_n350_), .Y(new_n352_));
  xor2   g277(.A0(new_n352_), .A1(new_n318_), .Y(new_n353_));
  and02  g278(.A0(new_n315_), .A1(new_n103_), .Y(new_n354_));
  and02  g279(.A0(new_n354_), .A1(new_n353_), .Y(new_n355_));
  and02  g280(.A0(new_n263_), .A1(CONT1_REG_8__SCAN_IN), .Y(new_n356_));
  and02  g281(.A0(CONT1_REG_2__SCAN_IN), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n357_));
  nor02  g282(.A0(new_n357_), .A1(CONT1_REG_3__SCAN_IN), .Y(new_n358_));
  and02  g283(.A0(new_n358_), .A1(new_n297_), .Y(new_n359_));
  nor02  g284(.A0(new_n359_), .A1(new_n199_), .Y(new_n360_));
  and02  g285(.A0(new_n360_), .A1(CONT1_REG_6__SCAN_IN), .Y(new_n361_));
  and02  g286(.A0(new_n361_), .A1(CONT1_REG_7__SCAN_IN), .Y(new_n362_));
  xor2   g287(.A0(new_n362_), .A1(CONT1_REG_8__SCAN_IN), .Y(new_n363_));
  and02  g288(.A0(new_n264_), .A1(new_n103_), .Y(new_n364_));
  and02  g289(.A0(new_n364_), .A1(new_n363_), .Y(new_n365_));
  or02   g290(.A0(new_n365_), .A1(new_n356_), .Y(new_n366_));
  or02   g291(.A0(new_n366_), .A1(new_n355_), .Y(new_n367_));
  or02   g292(.A0(new_n367_), .A1(new_n317_), .Y(new_n368_));
  or02   g293(.A0(new_n368_), .A1(new_n267_), .Y(U378));
  xor2   g294(.A0(CONT1_REG_7__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n370_));
  nand02 g295(.A0(new_n370_), .A1(new_n235_), .Y(new_n371_));
  or02   g296(.A0(new_n370_), .A1(new_n235_), .Y(new_n372_));
  and02  g297(.A0(new_n372_), .A1(new_n265_), .Y(new_n373_));
  and02  g298(.A0(new_n373_), .A1(new_n371_), .Y(new_n374_));
  and02  g299(.A0(CONT1_REG_7__SCAN_IN), .A1(new_n76_), .Y(new_n375_));
  xor2   g300(.A0(new_n375_), .A1(new_n268_), .Y(new_n376_));
  xor2   g301(.A0(new_n376_), .A1(new_n310_), .Y(new_n377_));
  and02  g302(.A0(new_n377_), .A1(new_n316_), .Y(new_n378_));
  xnor2  g303(.A0(new_n351_), .A1(new_n349_), .Y(new_n379_));
  and02  g304(.A0(new_n379_), .A1(new_n354_), .Y(new_n380_));
  and02  g305(.A0(new_n263_), .A1(CONT1_REG_7__SCAN_IN), .Y(new_n381_));
  xor2   g306(.A0(new_n361_), .A1(CONT1_REG_7__SCAN_IN), .Y(new_n382_));
  and02  g307(.A0(new_n382_), .A1(new_n364_), .Y(new_n383_));
  or02   g308(.A0(new_n383_), .A1(new_n381_), .Y(new_n384_));
  or02   g309(.A0(new_n384_), .A1(new_n380_), .Y(new_n385_));
  or02   g310(.A0(new_n385_), .A1(new_n378_), .Y(new_n386_));
  or02   g311(.A0(new_n386_), .A1(new_n374_), .Y(U377));
  and02  g312(.A0(new_n230_), .A1(new_n228_), .Y(new_n388_));
  xor2   g313(.A0(CONT1_REG_6__SCAN_IN), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n389_));
  nand02 g314(.A0(new_n389_), .A1(new_n388_), .Y(new_n390_));
  or02   g315(.A0(new_n389_), .A1(new_n388_), .Y(new_n391_));
  and02  g316(.A0(new_n391_), .A1(new_n265_), .Y(new_n392_));
  and02  g317(.A0(new_n392_), .A1(new_n390_), .Y(new_n393_));
  nand02 g318(.A0(new_n348_), .A1(new_n319_), .Y(new_n394_));
  and02  g319(.A0(new_n354_), .A1(new_n350_), .Y(new_n395_));
  and02  g320(.A0(new_n395_), .A1(new_n394_), .Y(new_n396_));
  xor2   g321(.A0(new_n274_), .A1(new_n268_), .Y(new_n397_));
  xor2   g322(.A0(new_n397_), .A1(new_n308_), .Y(new_n398_));
  and02  g323(.A0(new_n398_), .A1(new_n316_), .Y(new_n399_));
  nor02  g324(.A0(new_n263_), .A1(STATO_REG_2__SCAN_IN), .Y(new_n400_));
  and02  g325(.A0(new_n400_), .A1(STATO_REG_0__SCAN_IN), .Y(new_n401_));
  and02  g326(.A0(new_n401_), .A1(R_IN_REG_0__SCAN_IN), .Y(new_n402_));
  and02  g327(.A0(new_n402_), .A1(CONT_REG_5__SCAN_IN), .Y(new_n403_));
  and02  g328(.A0(new_n263_), .A1(CONT1_REG_6__SCAN_IN), .Y(new_n404_));
  xor2   g329(.A0(new_n360_), .A1(CONT1_REG_6__SCAN_IN), .Y(new_n405_));
  and02  g330(.A0(new_n405_), .A1(new_n364_), .Y(new_n406_));
  or02   g331(.A0(new_n406_), .A1(new_n404_), .Y(new_n407_));
  or02   g332(.A0(new_n407_), .A1(new_n403_), .Y(new_n408_));
  or02   g333(.A0(new_n408_), .A1(new_n399_), .Y(new_n409_));
  or02   g334(.A0(new_n409_), .A1(new_n396_), .Y(new_n410_));
  or02   g335(.A0(new_n410_), .A1(new_n393_), .Y(U376));
  xor2   g336(.A0(new_n320_), .A1(R_IN_REG_5__SCAN_IN), .Y(new_n412_));
  xor2   g337(.A0(new_n412_), .A1(new_n346_), .Y(new_n413_));
  and02  g338(.A0(new_n413_), .A1(new_n315_), .Y(new_n414_));
  nor02  g339(.A0(CONT1_REG_4__SCAN_IN), .A1(CONT1_REG_5__SCAN_IN), .Y(new_n415_));
  and02  g340(.A0(new_n415_), .A1(new_n358_), .Y(new_n416_));
  nor02  g341(.A0(new_n416_), .A1(new_n360_), .Y(new_n417_));
  and02  g342(.A0(new_n417_), .A1(new_n264_), .Y(new_n418_));
  and02  g343(.A0(new_n400_), .A1(R_IN_REG_5__SCAN_IN), .Y(new_n419_));
  or02   g344(.A0(new_n419_), .A1(new_n418_), .Y(new_n420_));
  or02   g345(.A0(new_n420_), .A1(new_n414_), .Y(new_n421_));
  and02  g346(.A0(new_n421_), .A1(new_n103_), .Y(new_n422_));
  xor2   g347(.A0(new_n210_), .A1(CONT1_REG_5__SCAN_IN), .Y(new_n423_));
  nand02 g348(.A0(new_n423_), .A1(new_n227_), .Y(new_n424_));
  or02   g349(.A0(new_n423_), .A1(new_n227_), .Y(new_n425_));
  and02  g350(.A0(new_n425_), .A1(new_n265_), .Y(new_n426_));
  and02  g351(.A0(new_n426_), .A1(new_n424_), .Y(new_n427_));
  nor02  g352(.A0(new_n278_), .A1(new_n276_), .Y(new_n428_));
  xnor2  g353(.A0(new_n428_), .A1(new_n306_), .Y(new_n429_));
  and02  g354(.A0(new_n429_), .A1(new_n316_), .Y(new_n430_));
  and02  g355(.A0(new_n401_), .A1(new_n100_), .Y(new_n431_));
  and02  g356(.A0(new_n431_), .A1(CONT_REG_5__SCAN_IN), .Y(new_n432_));
  and02  g357(.A0(new_n263_), .A1(CONT1_REG_5__SCAN_IN), .Y(new_n433_));
  and02  g358(.A0(new_n402_), .A1(CONT_REG_4__SCAN_IN), .Y(new_n434_));
  or02   g359(.A0(new_n434_), .A1(new_n433_), .Y(new_n435_));
  or02   g360(.A0(new_n435_), .A1(new_n432_), .Y(new_n436_));
  or02   g361(.A0(new_n436_), .A1(new_n430_), .Y(new_n437_));
  or02   g362(.A0(new_n437_), .A1(new_n427_), .Y(new_n438_));
  or02   g363(.A0(new_n438_), .A1(new_n422_), .Y(U375));
  xor2   g364(.A0(new_n323_), .A1(R_IN_REG_4__SCAN_IN), .Y(new_n440_));
  xor2   g365(.A0(new_n440_), .A1(new_n344_), .Y(new_n441_));
  and02  g366(.A0(new_n441_), .A1(new_n315_), .Y(new_n442_));
  xor2   g367(.A0(new_n358_), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n443_));
  and02  g368(.A0(new_n443_), .A1(new_n264_), .Y(new_n444_));
  and02  g369(.A0(new_n400_), .A1(R_IN_REG_4__SCAN_IN), .Y(new_n445_));
  or02   g370(.A0(new_n445_), .A1(new_n444_), .Y(new_n446_));
  or02   g371(.A0(new_n446_), .A1(new_n442_), .Y(new_n447_));
  and02  g372(.A0(new_n447_), .A1(new_n103_), .Y(new_n448_));
  xor2   g373(.A0(new_n201_), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n449_));
  xor2   g374(.A0(new_n449_), .A1(new_n225_), .Y(new_n450_));
  and02  g375(.A0(new_n450_), .A1(new_n265_), .Y(new_n451_));
  and02  g376(.A0(new_n263_), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n452_));
  xnor2  g377(.A0(CONT1_REG_4__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n453_));
  xor2   g378(.A0(new_n453_), .A1(new_n296_), .Y(new_n454_));
  and02  g379(.A0(new_n454_), .A1(new_n316_), .Y(new_n455_));
  or02   g380(.A0(new_n455_), .A1(new_n452_), .Y(new_n456_));
  and02  g381(.A0(new_n402_), .A1(CONT_REG_3__SCAN_IN), .Y(new_n457_));
  and02  g382(.A0(new_n431_), .A1(CONT_REG_4__SCAN_IN), .Y(new_n458_));
  or02   g383(.A0(new_n458_), .A1(new_n457_), .Y(new_n459_));
  or02   g384(.A0(new_n459_), .A1(new_n456_), .Y(new_n460_));
  or02   g385(.A0(new_n460_), .A1(new_n451_), .Y(new_n461_));
  or02   g386(.A0(new_n461_), .A1(new_n448_), .Y(U374));
  xor2   g387(.A0(new_n326_), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n463_));
  xor2   g388(.A0(new_n463_), .A1(new_n342_), .Y(new_n464_));
  and02  g389(.A0(new_n464_), .A1(new_n315_), .Y(new_n465_));
  xor2   g390(.A0(new_n357_), .A1(new_n207_), .Y(new_n466_));
  and02  g391(.A0(new_n466_), .A1(new_n264_), .Y(new_n467_));
  and02  g392(.A0(new_n400_), .A1(R_IN_REG_3__SCAN_IN), .Y(new_n468_));
  or02   g393(.A0(new_n468_), .A1(new_n467_), .Y(new_n469_));
  or02   g394(.A0(new_n469_), .A1(new_n465_), .Y(new_n470_));
  and02  g395(.A0(new_n470_), .A1(new_n103_), .Y(new_n471_));
  xor2   g396(.A0(new_n208_), .A1(CONT1_REG_3__SCAN_IN), .Y(new_n472_));
  nand02 g397(.A0(new_n472_), .A1(new_n223_), .Y(new_n473_));
  or02   g398(.A0(new_n472_), .A1(new_n223_), .Y(new_n474_));
  and02  g399(.A0(new_n474_), .A1(new_n265_), .Y(new_n475_));
  and02  g400(.A0(new_n475_), .A1(new_n473_), .Y(new_n476_));
  and02  g401(.A0(new_n431_), .A1(CONT_REG_3__SCAN_IN), .Y(new_n477_));
  and02  g402(.A0(new_n402_), .A1(CONT_REG_2__SCAN_IN), .Y(new_n478_));
  and02  g403(.A0(new_n263_), .A1(CONT1_REG_3__SCAN_IN), .Y(new_n479_));
  xnor2  g404(.A0(CONT1_REG_3__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n480_));
  xor2   g405(.A0(new_n480_), .A1(new_n294_), .Y(new_n481_));
  and02  g406(.A0(new_n481_), .A1(new_n316_), .Y(new_n482_));
  or02   g407(.A0(new_n482_), .A1(new_n479_), .Y(new_n483_));
  or02   g408(.A0(new_n483_), .A1(new_n478_), .Y(new_n484_));
  or02   g409(.A0(new_n484_), .A1(new_n477_), .Y(new_n485_));
  or02   g410(.A0(new_n485_), .A1(new_n476_), .Y(new_n486_));
  or02   g411(.A0(new_n486_), .A1(new_n471_), .Y(U373));
  and02  g412(.A0(new_n400_), .A1(R_IN_REG_2__SCAN_IN), .Y(new_n488_));
  xor2   g413(.A0(new_n329_), .A1(R_IN_REG_2__SCAN_IN), .Y(new_n489_));
  xor2   g414(.A0(new_n489_), .A1(new_n340_), .Y(new_n490_));
  and02  g415(.A0(new_n490_), .A1(new_n315_), .Y(new_n491_));
  xor2   g416(.A0(CONT1_REG_2__SCAN_IN), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n492_));
  and02  g417(.A0(new_n492_), .A1(new_n264_), .Y(new_n493_));
  or02   g418(.A0(new_n493_), .A1(new_n491_), .Y(new_n494_));
  or02   g419(.A0(new_n494_), .A1(new_n488_), .Y(new_n495_));
  and02  g420(.A0(new_n495_), .A1(new_n103_), .Y(new_n496_));
  and02  g421(.A0(CONT1_REG_2__SCAN_IN), .A1(new_n76_), .Y(new_n497_));
  xor2   g422(.A0(new_n497_), .A1(new_n291_), .Y(new_n498_));
  xor2   g423(.A0(new_n498_), .A1(new_n290_), .Y(new_n499_));
  and02  g424(.A0(new_n499_), .A1(new_n316_), .Y(new_n500_));
  and02  g425(.A0(new_n263_), .A1(CONT1_REG_2__SCAN_IN), .Y(new_n501_));
  xor2   g426(.A0(new_n210_), .A1(CONT1_REG_2__SCAN_IN), .Y(new_n502_));
  xor2   g427(.A0(new_n502_), .A1(new_n221_), .Y(new_n503_));
  and02  g428(.A0(new_n503_), .A1(new_n265_), .Y(new_n504_));
  or02   g429(.A0(new_n504_), .A1(new_n501_), .Y(new_n505_));
  or02   g430(.A0(new_n505_), .A1(new_n500_), .Y(new_n506_));
  and02  g431(.A0(new_n402_), .A1(CONT_REG_1__SCAN_IN), .Y(new_n507_));
  and02  g432(.A0(new_n431_), .A1(CONT_REG_2__SCAN_IN), .Y(new_n508_));
  or02   g433(.A0(new_n508_), .A1(new_n507_), .Y(new_n509_));
  or02   g434(.A0(new_n509_), .A1(new_n506_), .Y(new_n510_));
  or02   g435(.A0(new_n510_), .A1(new_n496_), .Y(U372));
  and02  g436(.A0(new_n400_), .A1(R_IN_REG_1__SCAN_IN), .Y(new_n512_));
  and02  g437(.A0(new_n264_), .A1(new_n284_), .Y(new_n513_));
  nor02  g438(.A0(new_n333_), .A1(new_n332_), .Y(new_n514_));
  xnor2  g439(.A0(new_n514_), .A1(new_n338_), .Y(new_n515_));
  and02  g440(.A0(new_n515_), .A1(new_n315_), .Y(new_n516_));
  or02   g441(.A0(new_n516_), .A1(new_n513_), .Y(new_n517_));
  or02   g442(.A0(new_n517_), .A1(new_n512_), .Y(new_n518_));
  and02  g443(.A0(new_n518_), .A1(new_n103_), .Y(new_n519_));
  xnor2  g444(.A0(CONT1_REG_1__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n520_));
  xor2   g445(.A0(new_n520_), .A1(new_n288_), .Y(new_n521_));
  and02  g446(.A0(new_n521_), .A1(new_n316_), .Y(new_n522_));
  and02  g447(.A0(new_n263_), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n523_));
  xor2   g448(.A0(new_n201_), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n524_));
  xor2   g449(.A0(new_n524_), .A1(new_n217_), .Y(new_n525_));
  and02  g450(.A0(new_n525_), .A1(new_n265_), .Y(new_n526_));
  or02   g451(.A0(new_n526_), .A1(new_n523_), .Y(new_n527_));
  or02   g452(.A0(new_n527_), .A1(new_n522_), .Y(new_n528_));
  and02  g453(.A0(new_n402_), .A1(CONT_REG_0__SCAN_IN), .Y(new_n529_));
  and02  g454(.A0(new_n431_), .A1(CONT_REG_1__SCAN_IN), .Y(new_n530_));
  or02   g455(.A0(new_n530_), .A1(new_n529_), .Y(new_n531_));
  or02   g456(.A0(new_n531_), .A1(new_n528_), .Y(new_n532_));
  or02   g457(.A0(new_n532_), .A1(new_n519_), .Y(U371));
  and02  g458(.A0(new_n400_), .A1(R_IN_REG_0__SCAN_IN), .Y(new_n534_));
  and02  g459(.A0(new_n264_), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n535_));
  xor2   g460(.A0(new_n334_), .A1(new_n100_), .Y(new_n536_));
  xor2   g461(.A0(new_n536_), .A1(new_n96_), .Y(new_n537_));
  and02  g462(.A0(new_n537_), .A1(new_n315_), .Y(new_n538_));
  or02   g463(.A0(new_n538_), .A1(new_n535_), .Y(new_n539_));
  or02   g464(.A0(new_n539_), .A1(new_n534_), .Y(new_n540_));
  and02  g465(.A0(new_n540_), .A1(new_n103_), .Y(new_n541_));
  and02  g466(.A0(new_n431_), .A1(CONT_REG_0__SCAN_IN), .Y(new_n542_));
  xor2   g467(.A0(R_IN_REG_3__SCAN_IN), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n543_));
  xor2   g468(.A0(new_n543_), .A1(new_n205_), .Y(new_n544_));
  and02  g469(.A0(new_n544_), .A1(new_n265_), .Y(new_n545_));
  and02  g470(.A0(new_n263_), .A1(CONT1_REG_0__SCAN_IN), .Y(new_n546_));
  and02  g471(.A0(CONT1_REG_0__SCAN_IN), .A1(STATO_REG_3__SCAN_IN), .Y(new_n547_));
  and02  g472(.A0(CONT1_REG_0__SCAN_IN), .A1(new_n76_), .Y(new_n548_));
  xor2   g473(.A0(new_n548_), .A1(new_n547_), .Y(new_n549_));
  and02  g474(.A0(new_n549_), .A1(new_n316_), .Y(new_n550_));
  or02   g475(.A0(new_n550_), .A1(new_n546_), .Y(new_n551_));
  or02   g476(.A0(new_n551_), .A1(new_n545_), .Y(new_n552_));
  or02   g477(.A0(new_n552_), .A1(new_n542_), .Y(new_n553_));
  or02   g478(.A0(new_n553_), .A1(new_n541_), .Y(U370));
  and02  g479(.A0(new_n119_), .A1(STATO_REG_3__SCAN_IN), .Y(new_n555_));
  and02  g480(.A0(new_n555_), .A1(CONT1_REG_8__SCAN_IN), .Y(new_n556_));
  and02  g481(.A0(new_n556_), .A1(new_n429_), .Y(new_n557_));
  inv01  g482(.A(new_n119_), .Y(new_n558_));
  and02  g483(.A0(new_n558_), .A1(X_OUT_REG_5__SCAN_IN), .Y(new_n559_));
  inv01  g484(.A(CONT1_REG_8__SCAN_IN), .Y(new_n560_));
  and02  g485(.A0(new_n119_), .A1(new_n560_), .Y(new_n561_));
  and02  g486(.A0(new_n561_), .A1(new_n268_), .Y(new_n562_));
  or02   g487(.A0(new_n562_), .A1(new_n559_), .Y(new_n563_));
  or02   g488(.A0(new_n563_), .A1(new_n557_), .Y(U369));
  and02  g489(.A0(new_n556_), .A1(new_n454_), .Y(new_n565_));
  and02  g490(.A0(new_n558_), .A1(X_OUT_REG_4__SCAN_IN), .Y(new_n566_));
  and02  g491(.A0(new_n555_), .A1(new_n560_), .Y(new_n567_));
  and02  g492(.A0(new_n567_), .A1(CONT1_REG_4__SCAN_IN), .Y(new_n568_));
  or02   g493(.A0(new_n568_), .A1(new_n566_), .Y(new_n569_));
  or02   g494(.A0(new_n569_), .A1(new_n565_), .Y(U368));
  and02  g495(.A0(new_n556_), .A1(new_n481_), .Y(new_n571_));
  and02  g496(.A0(new_n558_), .A1(X_OUT_REG_3__SCAN_IN), .Y(new_n572_));
  and02  g497(.A0(new_n567_), .A1(CONT1_REG_3__SCAN_IN), .Y(new_n573_));
  or02   g498(.A0(new_n573_), .A1(new_n572_), .Y(new_n574_));
  or02   g499(.A0(new_n574_), .A1(new_n571_), .Y(U367));
  and02  g500(.A0(new_n556_), .A1(new_n499_), .Y(new_n576_));
  and02  g501(.A0(new_n558_), .A1(X_OUT_REG_2__SCAN_IN), .Y(new_n577_));
  and02  g502(.A0(new_n561_), .A1(new_n291_), .Y(new_n578_));
  or02   g503(.A0(new_n578_), .A1(new_n577_), .Y(new_n579_));
  or02   g504(.A0(new_n579_), .A1(new_n576_), .Y(U366));
  and02  g505(.A0(new_n556_), .A1(new_n521_), .Y(new_n581_));
  and02  g506(.A0(new_n558_), .A1(X_OUT_REG_1__SCAN_IN), .Y(new_n582_));
  and02  g507(.A0(new_n567_), .A1(CONT1_REG_1__SCAN_IN), .Y(new_n583_));
  or02   g508(.A0(new_n583_), .A1(new_n582_), .Y(new_n584_));
  or02   g509(.A0(new_n584_), .A1(new_n581_), .Y(U365));
  and02  g510(.A0(new_n556_), .A1(new_n549_), .Y(new_n586_));
  and02  g511(.A0(new_n558_), .A1(X_OUT_REG_0__SCAN_IN), .Y(new_n587_));
  and02  g512(.A0(new_n561_), .A1(new_n547_), .Y(new_n588_));
  or02   g513(.A0(new_n588_), .A1(new_n587_), .Y(new_n589_));
  or02   g514(.A0(new_n589_), .A1(new_n586_), .Y(U364));
  and02  g515(.A0(STATO_REG_2__SCAN_IN), .A1(STATO_REG_1__SCAN_IN), .Y(new_n591_));
  and02  g516(.A0(new_n591_), .A1(STATO_REG_0__SCAN_IN), .Y(new_n592_));
  or02   g517(.A0(new_n592_), .A1(new_n118_), .Y(U360));
  and02  g518(.A0(new_n105_), .A1(STATO_REG_0__SCAN_IN), .Y(new_n594_));
  nor02  g519(.A0(new_n260_), .A1(new_n104_), .Y(new_n595_));
  or02   g520(.A0(new_n595_), .A1(new_n594_), .Y(U361));
  inv01  g521(.A(new_n106_), .Y(new_n597_));
  or02   g522(.A0(new_n117_), .A1(new_n597_), .Y(new_n598_));
  nor02  g523(.A0(new_n598_), .A1(new_n171_), .Y(new_n599_));
  nor02  g524(.A0(new_n252_), .A1(STATO_REG_1__SCAN_IN), .Y(new_n600_));
  or02   g525(.A0(new_n600_), .A1(new_n103_), .Y(new_n601_));
  or02   g526(.A0(STATO_REG_1__SCAN_IN), .A1(STATO_REG_0__SCAN_IN), .Y(new_n602_));
  or02   g527(.A0(new_n602_), .A1(new_n96_), .Y(new_n603_));
  and02  g528(.A0(new_n603_), .A1(STATO_REG_2__SCAN_IN), .Y(new_n604_));
  and02  g529(.A0(new_n604_), .A1(new_n601_), .Y(new_n605_));
  nor02  g530(.A0(new_n103_), .A1(STBI), .Y(new_n606_));
  and02  g531(.A0(new_n606_), .A1(new_n77_), .Y(new_n607_));
  or02   g532(.A0(new_n607_), .A1(new_n605_), .Y(new_n608_));
  or02   g533(.A0(new_n608_), .A1(new_n599_), .Y(U362));
  or02   g534(.A0(STATO_REG_2__SCAN_IN), .A1(STBI), .Y(new_n610_));
  or02   g535(.A0(R_IN_REG_1__SCAN_IN), .A1(STATO_REG_0__SCAN_IN), .Y(new_n611_));
  and02  g536(.A0(new_n611_), .A1(new_n172_), .Y(new_n612_));
  and02  g537(.A0(new_n612_), .A1(new_n610_), .Y(new_n613_));
  nand02 g538(.A0(new_n591_), .A1(new_n103_), .Y(new_n614_));
  nor02  g539(.A0(new_n614_), .A1(new_n254_), .Y(new_n615_));
  or02   g540(.A0(new_n615_), .A1(new_n119_), .Y(new_n616_));
  nor02  g541(.A0(new_n616_), .A1(new_n613_), .Y(new_n617_));
  nand02 g542(.A0(new_n617_), .A1(new_n598_), .Y(U363));
endmodule


