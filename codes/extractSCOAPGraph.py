import os,argparse,re
import random,shutil
import string
import math

from NetlistElements import BooleanGate,Net
from graph_tool.all import *

ndetectFile = ""
circuitName = ""
dumpDataPath = ""
gateDictionary = {}
netDictionary = {}
gateEdgeDictionary = {}
netVertexDictionary = {}
gateNameNetlistNameMapping = {}
gateNameNetlistNameOrdering = {}
logicLevelVtxDict = {}
offsetValue = 1



verilogFile = ""

wire_tag_list = ["input", "output"]
ignore_tag_list = ["module", "endmodule", "//","inout", "wire"]
ignore_list = ["GND","VCC","ZERO","ONE"]
punctuation_list = [",", ";"]
pi_list = None
po_list = None
bus_declaration_re = re.compile("[(\d+):(\d+)]")


netListGraph = Graph(directed=True)
edgeGateName = netListGraph.new_edge_property("string")
edgeGateType = netListGraph.new_edge_property("string")
netListGraph.edge_properties["gate_identifier"] = edgeGateName
netListGraph.edge_properties["gate_type"] = edgeGateType


vtxNetName = netListGraph.new_vertex_property("string")
vtxNetCC0 = netListGraph.new_vertex_property("int")
vtxNetCC1 = netListGraph.new_vertex_property("int")
vtxNetCO = netListGraph.new_vertex_property("int")
vtxNetLevel = netListGraph.new_vertex_property("int")
#vtxNetIsTestPointInserted = netListGraph.new_vertex_property("int")
vtxNetSA0Testability = netListGraph.new_vertex_property("float")
vtxNetSA1Testability = netListGraph.new_vertex_property("float")

netListGraph.vertex_properties["net_identifier"] = vtxNetName
netListGraph.vertex_properties["CC0"] = vtxNetCC0
netListGraph.vertex_properties["CC1"] = vtxNetCC1
netListGraph.vertex_properties["CO"] = vtxNetCO
netListGraph.vertex_properties["LogicLevel"] = vtxNetLevel
#netListGraph.vertex_properties["isTestPointInserted"] = vtxNetIsTestPointInserted
netListGraph.vertex_properties['sa0Testability'] = vtxNetSA0Testability
netListGraph.vertex_properties['sa1Testability'] = vtxNetSA1Testability


def updateCurrentLine(currentLineSegment,newLineSegment):
    new_line = newLineSegment.strip()
    end_of_line_found = False

    if len(new_line) > 0:
        if (new_line[-1] == ";"):
            end_of_line_found = True

    currentLineSegment = currentLineSegment + " " + new_line
    return currentLineSegment,end_of_line_found

def createNet(netName):
    global netDictionary

    if (not (netName in netDictionary.keys())):
        cktNet = Net(netName)
        isPI = 0
        isPO = 0
        if (netName in po_list):
            isPO = 1
        elif (netName in pi_list):
            isPI = 1
        cktNet.setPIorPOType(isPI,isPO)
        netDictionary[netName] = cktNet



def createBooleanGate(gateType,gateName):
    global gateDictionary
    gateObj = BooleanGate(gateName,gateType)
    gateDictionary[gateName] = gateObj



def processElementsOfBooleanGates(element_arr):
    full_component_string = "".join(element_arr[1:])  # has the effect of removing all whitespace from the original line
    component_arr = full_component_string.split(".")  # re-splitting on the .'s which indicate each IO of each component
    component_name = component_arr[0].replace("(", "")
    component_name = component_name.strip()
    componentType = element_arr[0]                    # GateType

    if(componentType in ignore_list):
        return
    createBooleanGate(componentType,component_name)
    outputInputNetList = []
    outputInputNetListOrdering = []


    for element_ind in range(-1, -len(component_arr),-1):
        element = component_arr[element_ind]
        #print(element)
        # Strip out unwanted characters
        new_element = element.replace("(", " ")
        new_element = new_element.replace(")", "")
        new_element = new_element.replace(",", "")
        new_element = new_element.replace(";", "")

        # Break into an array. First element will be IO name, second will be connected wire name
        new_element_arr = new_element.split()

        wire_name = new_element_arr[1]
        wire_order = new_element_arr[0]
        createNet(wire_name)
        outputInputNetList.append(wire_name)
        outputInputNetListOrdering.append(wire_order)

    gateNameNetlistNameMapping[component_name] = outputInputNetList
    gateNameNetlistNameOrdering[component_name] = outputInputNetListOrdering

def processPIandPOs(element_string):
    global pi_list,po_list
    element_string = element_string.replace(',', "")
    element_string = element_string.replace(';', "")
    element_arr = element_string.split()
    tag = element_arr[0]
    if (tag == "input"):
        pi_list = element_arr
    else:
        po_list = element_arr

def parse_verilog(infile_name):
    infile = open(infile_name, 'r')
    full_line = ""

    for line in infile:
        full_line,end_of_line_found = updateCurrentLine(full_line,line)

        if (not end_of_line_found):
            continue

        element_string = full_line
        full_line = ""

        element_arr = element_string.split()
        tag = element_arr[0]

        if (tag in ignore_tag_list):
            pass  # do nothing
        elif(tag in wire_tag_list):
            processPIandPOs(element_string)
        else:
            processElementsOfBooleanGates(element_arr)
            # tag not in wire_tag_list, therefore we're reading an actual component
            # tag is the component_type
            # We're assuming that ALL wires are already declared and added to the data structures in the previous section



def createVtx(netObj):
    global netListGraph,netVertexDictionary

    netName = netObj.getNetName()
    if(netName in netVertexDictionary.keys()):
        return netVertexDictionary[netName]

    newVtx = netListGraph.add_vertex()
    netVertexDictionary[netName] = newVtx

    vtxNetName[newVtx] = netObj.getNetName()
    vtxNetCC0[newVtx] = netObj.getIOParameters()[1]
    vtxNetCC1[newVtx] = netObj.getIOParameters()[2]
    vtxNetCO[newVtx] = netObj.getIOParameters()[3]
    vtxNetLevel[newVtx] = netObj.getIOParameters()[0]
    vtxNetSA0Testability[newVtx] = netObj.getIOParameters()[4]
    vtxNetSA1Testability[newVtx] = netObj.getIOParameters()[5]
    return newVtx

def createEdge(gateObj,vIn,vOut):
    global netListGraph,gateEdgeDictionary

    newEdge = netListGraph.add_edge(vIn,vOut)
    gateName = gateObj.getName()

    if(gateName in gateEdgeDictionary.keys()):
        gateEdgeDictionary[gateName].append(newEdge)
    else:
        gateEdgeDictionary[gateName] = [newEdge]

    edgeGateType[newEdge] = gateObj.getType()
    edgeGateName[newEdge] = gateName


def createCircuitDAG():
    global netListGraph,gateDictionary
    gateSet = gateDictionary.keys()
    for gateName in gateSet:
        gateObj = gateDictionary[gateName]
        inputOutputNameList = gateNameNetlistNameMapping[gateName]

        outNetObj = netDictionary[inputOutputNameList[0]]
        vOut = createVtx(outNetObj)

        for gateInpName in inputOutputNameList[1:]:
            inNetObj = netDictionary[gateInpName]
            vIn = createVtx(inNetObj)
            createEdge(gateObj,vIn,vOut)


def doTopologicalSortAndAssignLogicalLevels():
    global netListGraph,logicLevelVtxDict
    topologicalSortVtxList = topological_sort(netListGraph)
    logicLevelVtxDict[1] = []

    # We aren't updating the Gate and Net data structure again
    # We are here only updating
    for vtx in topologicalSortVtxList:
        # If vtx/net is not PI, compute logic level of net.
        # For PIs, logicLevel = 1
        # For nodes other than PIs, logic Level = max(levels of all input PIs)
        if(vtxNetLevel[vtx] < 0):
            if(len(netListGraph.get_in_neighbors(vtx)) == 0):
                print(vtxNetName[vtx])
            vtxNetLevel[vtx] = max([vtxNetLevel[inVtx] for inVtx in netListGraph.get_in_neighbors(vtx)])+1
            if(vtxNetLevel[vtx] in logicLevelVtxDict.keys()):
                logicLevelVtxDict[vtxNetLevel[vtx]].append(vtx)
            else:
                logicLevelVtxDict[vtxNetLevel[vtx]] = [vtx]
        elif(vtxNetLevel[vtx] == 1):
            # Push All the PIs
            logicLevelVtxDict[1].append(vtx)


def computeControlibilityOfNode(vtxUnderComputation):
        in_neighbours = netListGraph.get_in_neighbors(vtxUnderComputation)
        gateEdge = netListGraph.edge(in_neighbours[0], vtxUnderComputation)
        gateType = edgeGateType[gateEdge]
        inCC0 = [vtxNetCC0[input] for input in in_neighbours]
        inCC1 = [vtxNetCC1[input] for input in in_neighbours]
        if (gateType == "buf02"):
            #vtxNetCC0[vtxUnderComputation] = min(inCC0) + 1
            #vtxNetCC1[vtxUnderComputation] = min(inCC1) + 1
            vtxNetCC0[vtxUnderComputation] = min(inCC0) + offsetValue
            vtxNetCC1[vtxUnderComputation] = min(inCC1) + offsetValue

        elif (gateType == "inv01"):
            vtxNetCC0[vtxUnderComputation] = min(inCC1) + offsetValue
            vtxNetCC1[vtxUnderComputation] = min(inCC0) + offsetValue

        elif(gateType == "nor02"):
            vtxNetCC0[vtxUnderComputation] = min(inCC1) + offsetValue
            vtxNetCC1[vtxUnderComputation] = sum(inCC0) + offsetValue

        elif(gateType == "nand02"):
            vtxNetCC0[vtxUnderComputation] = sum(inCC1) + offsetValue
            vtxNetCC1[vtxUnderComputation] = min(inCC0) + offsetValue

        elif(gateType == "or02"):
            vtxNetCC0[vtxUnderComputation] = sum(inCC0) + offsetValue
            vtxNetCC1[vtxUnderComputation] = min(inCC1) + offsetValue

        elif(gateType == "and02"):
            vtxNetCC0[vtxUnderComputation] = min(inCC0) + offsetValue
            vtxNetCC1[vtxUnderComputation] = sum(inCC1) + offsetValue

        elif(gateType == "xor2"):
            vtxNetCC0[vtxUnderComputation] = min([inCC1[0]+inCC1[1],inCC0[0]+inCC0[1]]) + offsetValue
            vtxNetCC1[vtxUnderComputation] = min([inCC1[0]+inCC0[1],inCC0[0]+inCC1[1]]) + offsetValue

        elif(gateType == "xnor2"):
            vtxNetCC0[vtxUnderComputation] = min([inCC1[0] + inCC0[1], inCC0[0] + inCC1[1]]) + offsetValue
            vtxNetCC1[vtxUnderComputation] = min([inCC1[0] + inCC1[1], inCC0[0] + inCC0[1]]) + offsetValue


def computeObservabilityOfNode(vtxUnderComputation):
    fanoutObservability = []
    out_neighbours = netListGraph.get_out_neighbors(vtxUnderComputation)

    for outVtx in out_neighbours:
        gateEdge = netListGraph.edge(vtxUnderComputation,outVtx)
        gateType = edgeGateType[gateEdge]
        otherInVtxList = netListGraph.get_in_neighbours(outVtx)
        otherInVtx = otherInVtxList.tolist()#.remove(vtxUnderComputation)
        otherInVtx.remove(vtxUnderComputation)

        if(gateType == "and02" or gateType == "nand02"):
            observabilityFromCurrentGate = vtxNetCO[outVtx]+sum([vtxNetCC1[inVtx] for inVtx in otherInVtx])+offsetValue
            fanoutObservability.append(observabilityFromCurrentGate)

        elif (gateType == "or02" or gateType == "nor02"):
            observabilityFromCurrentGate = vtxNetCO[outVtx] + sum([vtxNetCC0[inVtx] for inVtx in otherInVtx]) + offsetValue
            fanoutObservability.append(observabilityFromCurrentGate)

        elif (gateType == "inv01" or gateType == "buf02"):
            observabilityFromCurrentGate = vtxNetCO[outVtx] + offsetValue
            fanoutObservability.append(observabilityFromCurrentGate)

        elif (gateType == "xor2" or gateType == "xnor2"):
            observabilityFromCurrentGate = vtxNetCO[outVtx] + sum([min(vtxNetCC0[inVtx],vtxNetCC1[inVtx]) for inVtx in otherInVtx]) + offsetValue
            fanoutObservability.append(observabilityFromCurrentGate)

    vtxNetCO[vtxUnderComputation] = min(fanoutObservability)




def computeSCOAPparameters():
    logicalLevelRank = sorted(logicLevelVtxDict.keys())
    reversedLLRank = reversed(logicalLevelRank)
    for rank in logicalLevelRank:
        listOfVtx = logicLevelVtxDict[rank]
        for vtx in listOfVtx:
            if(vtxNetCC0[vtx] < 1 or vtxNetCC1[vtx] < 1):
                computeControlibilityOfNode(vtx)

    for rank in reversedLLRank:
        listOfVtx = logicLevelVtxDict[rank]
        for vtx in listOfVtx:
            if(vtxNetCO[vtx] < 0):
                computeObservabilityOfNode(vtx)


def createGateNetInterConnections():
    for elem in gateDictionary.keys():
        boolGate = gateDictionary[elem]
        gateName = boolGate.getName()
        inputOutputNetNameList = gateNameNetlistNameMapping[gateName]
        inputOutputNetOrdering = gateNameNetlistNameOrdering[gateName]
        inputNetObjs = [netDictionary[inputNetName] for inputNetName in inputOutputNetNameList[1:]]
        outputNetObj = netDictionary[inputOutputNetNameList[0]]
        boolGate.assignInputAndOutput(inputNetObjs,outputNetObj,inputOutputNetOrdering)
        for inpObj in inputNetObjs:
            inpObj.addDrivingGate(boolGate)


def parseNdetectDataFileNApplyLabel(ndetectFile):
    global netListGraph,gateEdgeDictionary
    safNDetectionFileLines = open(ndetectFile,'r+').readlines()
    for line in safNDetectionFileLines[1:]:
        rowEntries = line.strip('\n').split(',')
        safType = int(rowEntries[0])
        testabilityValue = float(rowEntries[2])/1020000 # 1 million 20k testcases
        ioportID = rowEntries[1]
        if(ioportID.__contains__(".")):
            ioportEntries = ioportID.split('.')
            gateID = ioportEntries[0]
            portID = ioportEntries[1]
            if(gateID not in gateDictionary.keys()):
                continue
            gateObj = gateDictionary[gateID]
            targetNetObj = gateObj.getInputNetHandle(portID)
            targetVtx = netVertexDictionary[targetNetObj.getNetName()]
        else:
            if(ioportID not in netVertexDictionary.keys()):
                continue
            targetVtx = netVertexDictionary[ioportID]

        if(safType == 0):
            vtxNetSA0Testability[targetVtx] = testabilityValue
        else:
            vtxNetSA1Testability[targetVtx] = testabilityValue


def parseDataFileAndPopulateDataStructures():
    parse_verilog(verilogFile)
    createGateNetInterConnections()


def setGlobalAndEnvironmentVars(cmdArgs):
    global verilogFile,ndetectFile,circuitName,dumpDataPath
    if(not(os.path.exists(cmdArgs.bench) or os.path.exists(cmdArgs.ndetect) or os.path.exists(cmdArgs.dump))):
        print(cmdArgs.bench,cmdArgs.dump,cmdArgs.ndetect)
        print("Either verilog benchmark file or SCOAP data is missing. Please rerun with appropriate args")
        exit(1)
    verilogFile = cmdArgs.bench
    ndetectFile = cmdArgs.ndetect
    dumpDataPath = cmdArgs.dump
    circuitName = os.path.basename(verilogFile)

def parseCmdLineArgs():
    parser = argparse.ArgumentParser(prog='SCOAP_EXTRACT', description="SCOAP data extractor")
    parser.add_argument('--version', action='version', version='1.0.0')
    parser.add_argument('--bench', required=True, help="Path of Verilog Benchmark File")
    parser.add_argument('--ndetect', required=True, help="Path of  of benchmark")
    parser.add_argument('--dump', required=True, help="Path where graph data would be dumped")
    return parser.parse_args()


def dumpNetListGraphInformation():
    graphmlFileOfSCOAPdata = dumpDataPath+os.sep+"scoapData_" + circuitName + ".graphml"
    dotFileOfSCOAPdata = dumpDataPath+os.sep+"scoapData_" + circuitName + ".dot"
    netListGraph.save(graphmlFileOfSCOAPdata, fmt='graphml')
    netListGraph.save(dotFileOfSCOAPdata, fmt='dot')


def main():
    cmdArgs = parseCmdLineArgs()
    setGlobalAndEnvironmentVars(cmdArgs)
    parseDataFileAndPopulateDataStructures()
    createCircuitDAG()
    doTopologicalSortAndAssignLogicalLevels()
    computeSCOAPparameters()
    #parseTpiDataFileNApplyLabel(tpiFile)
    parseNdetectDataFileNApplyLabel(ndetectFile)
    dumpNetListGraphInformation()


if __name__ == '__main__':
    main()