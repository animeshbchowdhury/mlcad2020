import re
readFile = open('c432_synth_rf0.rpt','r+')
lines = readFile.readlines()
readFile.close()

dictOfKey = {}
numLines = len(lines)
i = 3
while(i < numLines):
    line = lines[i]
    words = re.findall('\w+',line)
    if(line.__contains__('DS')):
        safType = words[0]
        if(len(words) > 3):
            netID = words[-2]+"."+words[-1]
        else:
            netID = words[-1]
        nextLine = re.findall('\w+',lines[i+1])
        numDetect = nextLine[-1]
        print('DS',safType,netID,numDetect)
        keyValue = netID+"."+safType
        if keyValue in dictOfKey.keys():
            dictOfKey[keyValue] += int(numDetect)
        else:
            dictOfKey[keyValue] = int(numDetect)
        i+=2
    elif(line.__contains__('UO')):
        safType = words[0]
        if (len(words) > 3):
            netID = words[-2] + "." + words[-1]
        else:
            netID = words[-1]
        keyValue = netID+"."+safType
        if (not (keyValue in dictOfKey.keys())):
            dictOfKey[keyValue] = 0
        i+=1
    elif(line.__contains__('EQ')):
        safType = words[0]
        if (len(words) > 3):
            netID = words[-2] + "." + words[-1]
        else:
            netID = words[-1]
        keyValue = netID + "." + safType
        if (not (keyValue in dictOfKey.keys())):
            dictOfKey[keyValue] = 0
        nextLine = lines[i+1]
        if(nextLine.__contains__("num_detection")):
            nextLine = re.findall('\w+', lines[i + 1])
            numDetect = nextLine[-1]
            print('EQ', safType, netID, numDetect)
            dictOfKey[keyValue] += int(numDetect)
            i+=2
        else:
            i+=1
    else:
        i+=1

print(dictOfKey)