import argparse,os


'''
"epfl_arith" : sqrt going to SCOAP overflow.
               hyp is taking long time for test-point insertion.
'''
reposRoot = "/home/animesh/currentResearch/mlcad2020"
cellLibraryPath = os.path.join(reposRoot,'testData','cellLibrary.atpg')
benchmarkCktRootPath = os.path.join(reposRoot,'testData','netlists')
benchKey = ""
dataDumpRootPath = os.path.join(reposRoot,'dumpData')
verilogFileExtension = ".v"
doFileExtension = ".dofile"
numBatch = 4000
benchmarkDictionary = { "iscas85" : {"c432_synth" : numBatch, "c499_synth" : numBatch, "c1908_synth" : numBatch,"c3540_synth":numBatch,"c7552_synth":numBatch},
                        "iscas89" : {"s526_synth" : numBatch, "s832_synth" : numBatch, "s838_synth" : numBatch,"s1494_synth" : numBatch,"s9234_synth" :numBatch,"s15850_synth" : numBatch
                            ,"s38584_synth": numBatch},
                        "itc99"   : {"b10_synth" : numBatch,"b11_synth" : numBatch,"b12_synth" : numBatch,"b14_synth" : numBatch,"b15_synth" : numBatch,"b17_synth" : numBatch,"b18_synth" : numBatch
                                     ,"b19_synth" : numBatch,"b20_synth" : numBatch,"b21_synth" : numBatch,"b22_synth" : numBatch},
                        "epfl_arith" : {"adder_synth" : numBatch,"bar_synth" : numBatch,"div_synth" : numBatch,"log2_synth" :numBatch,"max_synth" : numBatch,
                                        "multiplier_synth":numBatch,"sin_synth":numBatch},
                        "epfl_rc" : {"arbiter_synth" : numBatch,"cavlc_synth" : numBatch, "ctrl_synth" :numBatch,"dec_synth" : numBatch,"i2c_synth":numBatch,"int2float_synth":numBatch,
                                     "mem_ctrl_synth":numBatch,"priority_synth":numBatch,"router_synth":numBatch,"voter_synth":numBatch},
                      }

def main():
    cmdArgs = parseCmdLineArgs()
    setGlobalAndEnvironmentVars(cmdArgs)
    print(benchKey)
    benchmarkCktList = benchmarkDictionary[benchKey]
    for ckt in benchmarkCktList:
        benchmarkPath = os.path.join(benchmarkCktRootPath,benchKey,ckt+verilogFileExtension)
        ndetectDataFile = os.path.join(dataDumpRootPath,benchKey,ckt,ckt+"_ndetect.csv")
        trainingGraphGenRunCmd = "/usr/bin/python3 extractSCOAPGraph.py --bench "+benchmarkPath+" " \
                                " --ndetect "+ndetectDataFile+" --dump "+os.path.join(dataDumpRootPath,benchKey,ckt)
        os.system(trainingGraphGenRunCmd)
        print("\ncompleting"+ckt)




def setGlobalAndEnvironmentVars(cmdArgs):
    global benchKey
    if(not (cmdArgs.benchkey in benchmarkDictionary.keys())):
        print("Benchmark key is not available. Please rerun with appropriate args")
        exit(1)
    benchKey = cmdArgs.benchkey


def parseCmdLineArgs():
    parser = argparse.ArgumentParser(prog='TRAINING_DATA_GEN', description="Training data generation using SCOAP")
    parser.add_argument('--version', action='version', version='1.0.0')
    parser.add_argument('--benchkey', required=True, help="Category of benchmark - (iscas85,iscas89,itc99,epfl_rc,epfl_arith)")
    return parser.parse_args()

if __name__ == '__main__':
    main()
