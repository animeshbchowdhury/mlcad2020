import math
class Net:

    def __init__(self,netName):
        self.name = netName
        self.controlibility_1 = -1
        self.observability = -1
        self.controlibility_0 = -1
        self.isPI = 0
        self.isPO = 0
        self.logicalLevel = -1
        self.sa0Testability = 0
        self.sa1Testability = 0
        self.fanoutGates = []

    def setPIorPOType(self,isPI,isPO):
        self.isPI = isPI
        self.isPO = isPO
        if(self.isPI):
            self.logicalLevel = 1
            self.controlibility_1 = 1
            self.controlibility_0 = 1

        if(self.isPO):
            self.observability = 0

    def getNetType(self):
        return self.isPI,self.isPO

    def setLogicialDepth(self,depth):
        self.depth = depth

    def getNetName(self):
        return self.name

    def getFanoutGates(self):
        return self.fanoutGates

    def addDrivingGate(self,gateObj):
        self.fanoutGates.append(gateObj)

    def setSCOAPparameters(self,cc0,cc1,co):
        self.controlibility_0 = cc0
        self.controlibility_1 = cc1
        self.observability = co

    def getIOParameters(self):
        return [self.logicalLevel,self.controlibility_0,self.controlibility_1,self.observability,self.sa0Testability,self.sa1Testability]

    def getLogicalLevel(self):
        return self.logicalLevel


class BooleanGate:

    def __init__(self,gateName,gateType):
        self.name = gateName
        self.gateType = gateType
        self.inputNets = []
        self.outputNet = None
        self.observability = 0
        self.outputInputOrdering = [] # Reverse Ordering

    def assignInputAndOutput(self,inputs,output,outputInputOrdering):
        self.inputNets = inputs
        self.outputNet = output
        self.outputInputOrdering = outputInputOrdering

    def getInputNetHandle(self,IOportID):
        for i in range(len(self.outputInputOrdering)):
            if(IOportID == self.outputInputOrdering[i]):
                return self.inputNets[i-1] #Extra PO is added in the top of list for inputOutputOrdering

    def setObservability(self,scoapObservability):
        self.observability = scoapObservability

    def getObservability(self):
        return self.observability

    def getName(self):
        return self.name

    def getType(self):
        return self.gateType

    def getInputs(self):
        return self.inputNets

    def getOutput(self):
        return self.outputNet