import argparse,os,shutil,re

reposRoot = "/home/animesh/currentResearch/mlcad2020"
cellLibraryPath = os.path.join(reposRoot,'testData','cellLibrary.atpg')
benchmarkCktRootPath = os.path.join(reposRoot,'testData','netlists')
benchKey = ""
dataDumpRootPath = os.path.join(reposRoot,'dumpData')
verilogFileExtension = ".v"
doFileExtension = ".dofile"
numBatch = 4000

benchmarkDictionary = { "iscas85" : {"c432_synth" : numBatch, "c499_synth" : numBatch, "c1908_synth" : numBatch,"c3540_synth":numBatch,"c7552_synth":numBatch},
                        "iscas89" : {"s526_synth" : numBatch, "s832_synth" : numBatch, "s838_synth" : numBatch,"s1494_synth" : numBatch,"s9234_synth" :numBatch,"s15850_synth" : numBatch
                            ,"s38584_synth": numBatch},
                        "itc99"   : {"b10_synth" : numBatch,"b11_synth" : numBatch,"b12_synth" : numBatch,"b14_synth" : numBatch,"b15_synth" : numBatch,"b17_synth" : numBatch,"b18_synth" : numBatch
                                     ,"b19_synth" : numBatch,"b20_synth" : numBatch,"b21_synth" : numBatch,"b22_synth" : numBatch},
                        "epfl_arith" : {"adder_synth" : numBatch,"bar_synth" : numBatch,"div_synth" : numBatch,"log2_synth" :numBatch,"max_synth" : numBatch,
                                        "multiplier_synth":numBatch,"sin_synth":numBatch},
                        "epfl_rc" : {"arbiter_synth" : numBatch,"cavlc_synth" : numBatch, "dec_synth" : numBatch,"i2c_synth":numBatch,"int2float_synth":numBatch,
                                     "mem_ctrl_synth":numBatch,"priority_synth":numBatch,"router_synth":numBatch,"voter_synth":numBatch},
                      }

def dumpNdetectDataForCkt(num,dumpPath,ckt):
    benchmarkReportDir = os.path.join(dumpPath, "reports")
    dictOfKey = {}
    for i in range(num):
        faultFilePath = os.path.join(benchmarkReportDir,ckt+"_rf"+str(i)+".rpt")
        faultFileHandler = open(faultFilePath,'r+')
        faultFileLines = faultFileHandler.readlines()
        faultFileHandler.close()
        numFaultLines = len(faultFileLines)
        idx = 3
        while(idx < numFaultLines):
            line = faultFileLines[idx]
            words = re.findall('\w+', line)
            if (line.__contains__('DS')):
                safType = words[0]
                if (len(words) > 3):
                    netID = words[-2] + "." + words[-1]
                else:
                    netID = words[-1]
                nextLine = re.findall('\w+', faultFileLines[idx + 1])
                numDetect = nextLine[-1]
                #print('DS', safType, netID, numDetect)
                keyValue = netID + "-" + safType
                if keyValue in dictOfKey.keys():
                    dictOfKey[keyValue] += int(numDetect)
                else:
                    dictOfKey[keyValue] = int(numDetect)
                idx += 2
            elif (line.__contains__('UO')):
                safType = words[0]
                if (len(words) > 3):
                    netID = words[-2] + "." + words[-1]
                else:
                    netID = words[-1]
                keyValue = netID + "-" + safType
                if (not (keyValue in dictOfKey.keys())):
                    dictOfKey[keyValue] = 0
                idx += 1
            elif (line.__contains__('EQ')):
                safType = words[0]
                if (len(words) > 3):
                    netID = words[-2] + "." + words[-1]
                else:
                    netID = words[-1]
                keyValue = netID + "-" + safType
                if (not (keyValue in dictOfKey.keys())):
                    dictOfKey[keyValue] = 0
                nextLine = faultFileLines[idx + 1]
                if (nextLine.__contains__("num_detection")):
                    nextLineWord = re.findall('\w+', nextLine)
                    numDetect = nextLineWord[-1]
                    #print('EQ', safType, netID, numDetect)
                    dictOfKey[keyValue] += int(numDetect)
                    idx += 2
                else:
                    idx += 1
            else:
                idx += 1

    benchmarkConsolidatedReportFile = os.path.join(dumpPath, ckt + '_' + 'ndetect.csv')
    benchConsolidatedReportFileHandler = open(benchmarkConsolidatedReportFile, 'w+')
    benchConsolidatedReportFileHandler.write("safType,netID,numDetections\n")
    faultKeyList = dictOfKey.keys()
    for faultKey in faultKeyList:
        faultKeyWords = faultKey.split('-')
        safType = faultKeyWords[1]
        gateID = faultKeyWords[0]
        numDetections = dictOfKey[faultKey]
        benchConsolidatedReportFileHandler.write(safType+","+gateID+","+str(numDetections)+"\n")
    benchConsolidatedReportFileHandler.close()

def main():
    cmdArgs = parseCmdLineArgs()
    setGlobalAndEnvironmentVars(cmdArgs)

    print(benchKey)
    benchmarkCktList = benchmarkDictionary[benchKey].keys()
    for ckt in benchmarkCktList:
        batchOf255Vectors = benchmarkDictionary[benchKey][ckt]
        benchmarkDumpData = os.path.join(dataDumpRootPath,benchKey,ckt)
        dumpNdetectDataForCkt(batchOf255Vectors,benchmarkDumpData,ckt)


def setGlobalAndEnvironmentVars(cmdArgs):
    global benchmarkCktRootPath,benchKey
    if(not (cmdArgs.benchkey in benchmarkDictionary.keys())):
        print("Benchmark key is not available. Please rerun with appropriate args")
        exit(1)
    benchKey = cmdArgs.benchkey


def parseCmdLineArgs():
    parser = argparse.ArgumentParser(prog='TRAINING_DATA_GEN', description="Training data generation using SCOAP")
    parser.add_argument('--version', action='version', version='1.0.0')
    parser.add_argument('--benchkey', required=True, help="Category of benchmark - (iscas85,iscas89,itc99,epfl_rc,epfl_arith)")
    return parser.parse_args()

if __name__ == '__main__':
    main()
