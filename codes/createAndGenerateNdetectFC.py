import argparse,os,shutil

reposRoot = "/home/abc586/currentResearch/mlcad2020"
cellLibraryPath = os.path.join(reposRoot,'testData','cellLibrary.atpg')
benchmarkCktRootPath = os.path.join(reposRoot,'testData','netlists')
benchKey = ""
dataDumpRootPath = os.path.join(reposRoot,'dumpData')
verilogFileExtension = ".v"
doFileExtension = ".dofile"
numBatch = 4000

benchmarkDictionary = { "iscas85" : {"c432_synth" : numBatch, "c499_synth" : numBatch, "c1908_synth" : numBatch,"c3540_synth":numBatch,"c7552_synth":numBatch},
                        "iscas89" : {"s526_synth" : numBatch, "s832_synth" : numBatch, "s838_synth" : numBatch,"s1494_synth" : numBatch,"s9234_synth" :numBatch,"s15850_synth" : numBatch
                            ,"s38584_synth": numBatch},
                        "itc99"   : {"b10_synth" : numBatch,"b14_synth" : numBatch,"b19_synth" : numBatch,"b15_synth" : numBatch,"b22_synth" : numBatch},
                        "epfl_arith" : {"adder_synth" : numBatch,"bar_synth" : numBatch,"div_synth" : numBatch,"log2_synth" :numBatch,"max_synth" : numBatch,
                                        "multiplier_synth":numBatch,"sin_synth":numBatch},
                        "epfl_rc" : {"arbiter_synth" : numBatch,"cavlc_synth" : numBatch, "ctrl_synth" :numBatch,"dec_synth" : numBatch,"i2c_synth":numBatch,"int2float_synth":numBatch,
                                     "mem_ctrl_synth":numBatch,"priority_synth":numBatch,"router_synth":numBatch,"voter_synth":numBatch},
                      }

def returnLoopingPara(num,dumpPath,ckt):
    paraToWrite = ""
    for i in range(num):
        repeatedRandomTest = "delete_faults -all\n" \
                             "read_faults "+ckt+".flt\n" \
                             "simulate_patterns -source random\n" \
                             "report_faults > "+dumpPath+os.sep+ckt+"_rf"+str(i)+".rpt\n" \
                             "report_statistics > "+dumpPath+os.sep+ckt+"_rs"+str(num)+".rpt\n"
        paraToWrite+=repeatedRandomTest
    return paraToWrite

def main():
    cmdArgs = parseCmdLineArgs()
    setGlobalAndEnvironmentVars(cmdArgs)

    print(benchKey)
    benchmarkCktList = benchmarkDictionary[benchKey].keys()
    for ckt in benchmarkCktList:
        batchOf255Vectors = benchmarkDictionary[benchKey][ckt]
        benchmarkPath = os.path.join(benchmarkCktRootPath,benchKey,ckt+verilogFileExtension)
        benchmarkDumpData = os.path.join(dataDumpRootPath,benchKey,ckt)
        benchmarkReportDir = os.path.join(benchmarkDumpData, "reports")
        if os.path.exists(benchmarkDumpData):
            shutil.rmtree(benchmarkDumpData)
            if os.path.exists(benchmarkReportDir):
                shutil.rmtree(benchmarkReportDir)
        os.mkdir(benchmarkDumpData)
        os.mkdir(benchmarkReportDir)
        benchmarkDataDoFile =  os.path.join(benchmarkDumpData,ckt+'_'+'ndetect.dofile')
        benchDoFileHandler = open(benchmarkDataDoFile,'w+')
        paraGraph1 = "set_context patterns -scan\n" \
                    "read_verilog " + benchmarkPath + " \n" \
                    "read_cell_library " + cellLibraryPath + "\n" \
                    "set_current_design top\n" \
                    "set_system_mode analysis\n" \
                    "add_fault -all\n" \
                    "set_fault_type stuck\n" \
                    "set_multiple_detection -Desired_atpg_detections 255\n" \
                    "set_random_patterns 255\n" \
                    "write_faults "+ckt+".flt -replace\n"
        paraGraph2 = returnLoopingPara(batchOf255Vectors,benchmarkReportDir,ckt)
        paragraph = paraGraph1+paraGraph2+"exit\n"
        benchDoFileHandler.write(paragraph)
        benchDoFileHandler.close()

        doFileRunCmd = "tessent -shell -dofile "+benchmarkDataDoFile
        os.system(doFileRunCmd)


def setGlobalAndEnvironmentVars(cmdArgs):
    global benchmarkCktRootPath,benchKey
    if(not (cmdArgs.benchkey in benchmarkDictionary.keys())):
        print("Benchmark key is not available. Please rerun with appropriate args")
        exit(1)
    benchKey = cmdArgs.benchkey


def parseCmdLineArgs():
    parser = argparse.ArgumentParser(prog='TRAINING_DATA_GEN', description="Training data generation using SCOAP")
    parser.add_argument('--version', action='version', version='1.0.0')
    parser.add_argument('--benchkey', required=True, help="Category of benchmark - (iscas85,iscas89,itc99,epfl_rc,epfl_arith)")
    return parser.parse_args()

if __name__ == '__main__':
    main()
